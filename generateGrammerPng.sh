#!/bin/bash

# Requires pdflatex ('brew install basictex' on macos)
# Requires gs ('brew install gs' on macos)

rm Grammar.pdf
pdflatex Grammar.tex
gs -sDEVICE=pnggray -sBATCH -sOutputFile=Grammar.png -dNOPAUSE -r1200 Grammar.pdf