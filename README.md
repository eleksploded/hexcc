# JHexCC
Recreation of HexCC but in Java. The old C++ version is still [here](https://gitlab.com/eleksploded/hexcc/-/tree/main?ref_type=heads)

## The Language
Just like the C++ counterpart, JHexCC uses HexC, a C inspired language designed for HexCC. The goal was for it to write just like C,
with assembly replaced with *raw* blocks.

The grammar is slightly different from the original HexCC, and can be seen [here](Grammar.png). It may not be 100% correct, and the best way to see the language is the [example file](examples.txt).

### Raw Blocks
Raw blocks are HexC's version of C's inline assembly. They are declared with the raw keyword follow by an open and bracket (just like they were in HexCC).

Inside the brackets, Hex patterns can be written, separated by a newline. Additionally, `#` and `$` can be used to save or load an identifier.
- `#(identifier)` - Save's an Iota to the given identifier, removing the Iota from the stack in the process.
- `$(identifier)` - Loads the given identifier's Iota to the top of the stack

Raw blocks cannot create identifiers, so any used inside the block should be declared before (see the [example file](examples.txt) for an example).

Additionally, raw blocks allow you to easy embed a Pattern, using `/`. 
This gives a place to replace said pattern with an Iota, or keep it to just embed a pattern.
This is achieved by putting `/(Pattern)` in a raw block, just as the identifier reading and writing would be.


## Compilation Steps
Compilation of HexC to Hex takes place in a few steps seen below. The output of each step can be seen in the [example file](examples.txt).

### [Tokenization](src/me/juliadev/jhexcc/lex/Lexical.java)
#### Source -> Tokens
Works basically identical to it's C++ counterpart. Unlike C++ however, it is able to keep tokens linked with the file they came from for error locating.
Additionally, it now (theoretically) supports character and string literals, for use with MoteIotas, even though they are not supported elsewhere yet.

### [Parsing](src/me/juliadev/jhexcc/parse/Parser.java) 
#### Tokens -> ParseTree
The parse tree is much easier to follow in Java than in C++. Every program has a [NodePgrm](src/me/juliadev/jhexcc/parse/nodes/NodePgrm.java), which contains all
the declarations within the program, as well as what file they came from. 

Additionally, operators now have [weighting on them](src/me/juliadev/jhexcc/parse/ExprParser.java), meaning the compiler respects an order of operations when compiling.
This is achieved with [Precedence Climbing](https://eli.thegreenplace.net/2012/08/02/parsing-expressions-by-precedence-climbing). 

### [IrHexC Code Generation](src/me/juliadev/jhexcc/codegen/ir_hexc/IntermediateHexCGenerator.java) 
#### ParseTree -> IrHexC
The parse tree is then converted into IrHexC, an **I**ntermediate **r**epresentation of **HexC**.
This is a simplified representation of the final Hex, used for easier tracing and optimization. There is a very limited
selection of 'actions' as I call them available to IrHexC, mostly to do with identifier management. Raw blocks are passed though
IrHexC as [IrHexCRawHex](src/me/juliadev/jhexcc/codegen/ir_hexc/IrHexCRawHex.java) actions containing the corresponding hex.

### [Optimization](src/me/juliadev/jhexcc/optimization/Optimizer.java) 
#### IrHexC -> IrHexC
Optimization aims to reduce the number of patterns in the final code. While there aren't many yet, I hope to add more in the future.     
*See [here](src/me/juliadev/jhexcc/optimization/optimizations.md) for the current optimizations.*

### [Tracing](src/me/juliadev/jhexcc/verify/TraceIR.java) 
#### (No Change)
While not implemented yet, the goal is a system to trace through both the IrHexC and final Hex with stack simulation to verify the output.
This will hopefully catch both errors in raw blocks, and reaching the meta-eval limit.

### [Hex Code Generation](src/me/juliadev/jhexcc/codegen/hex/HexGenerator.java) 
#### (IrHexC -> Hex)
This finally converts the IrHexC into actual patterns for use with HexCasting!