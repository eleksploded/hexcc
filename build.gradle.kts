plugins {
    id("java")
    id("io.freefair.lombok") version "8.7.1"
    id("com.gradleup.shadow") version "8.3.0"
    id("application")
}

repositories {
    mavenCentral()
}

application {
    mainClass.set("me.juliadev.jhexcc.Main")
}

dependencies {
    implementation("info.picocli:picocli:4.7.6")
    implementation("com.squareup.moshi:moshi:1.15.1")
    testImplementation("junit:junit:4.13")
}

tasks.build {
    dependsOn("shadowJar")
}