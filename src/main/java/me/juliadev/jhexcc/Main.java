package me.juliadev.jhexcc;

import me.juliadev.jhexcc.codegen.GeneratorError;
import me.juliadev.jhexcc.codegen.hex.HexGenerator;
import me.juliadev.jhexcc.codegen.hex.HexPattern;
import me.juliadev.jhexcc.codegen.ir_hexc.IrHexCProgram;
import me.juliadev.jhexcc.lex.Lexical;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.TokenizationError;
import me.juliadev.jhexcc.optimization.Optimization;
import me.juliadev.jhexcc.optimization.Optimizer;
import me.juliadev.jhexcc.output.CustomMappingExporter;
import me.juliadev.jhexcc.output.FormatterError;
import me.juliadev.jhexcc.output.PatternExporter;
import me.juliadev.jhexcc.parse.ParseError;
import me.juliadev.jhexcc.parse.Parser;
import me.juliadev.jhexcc.parse.nodes.NodePgrm;
import me.juliadev.jhexcc.util.Pair;
import picocli.CommandLine;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

@CommandLine.Command(name = "jhexcc", mixinStandardHelpOptions = true, version = "jhexcc 1.0",
        description = "HexC Compiler for Hex Casting")
public class Main implements Callable<Integer> {
    private static final int SUCCESS = 0;
    private static final int ARG_ERROR = 1;
    private static final int TOKEN_ERROR = 2;
    private static final int PARSE_ERROR = 3;
    private static final int CODEGEN_ERROR = 4;
    private static final int FORMAT_ERROR = 5;
    private static final int IO_ERROR = 8;
    
    @CommandLine.Option(names = {"--listOptimizations"}, arity = "0", help = true, description = "Lists all available optimizations and exits")
    private boolean listOptimizations;
    
    @CommandLine.Option(names = {"-i", "--irhexc"}, arity = "0", description = "Outputs IrHexC to the output file, with '.irhexc' appended to the filename")
    private boolean outputIrHexC;
    
    @CommandLine.Parameters(arity = "1..*", paramLabel = "source(s)", description = "Source File(s) to compile.")
    private File[] inputFiles;
    
    @CommandLine.Option(names = {"-o", "--out"}, description = "Output file name of compiled hex", defaultValue = "a.hex")
    private String outputFile;
    
    @CommandLine.Option(names = {"-p", "--optimizations"}, arity = "0..*", description = "All optimizations to run")
    private String[] optimizations;
    
    @CommandLine.Option(names = {"--optPasses"}, description = "Max passes to be made by the optimizer (-1 for no max)", defaultValue = "-1")
    private int maxOptimizerPasses = -1;
    
    @CommandLine.Option(names = {"-f", "--format"}, description = "Output format of the hex", defaultValue = "TRANSLATION")
    private OutputFormat format;
    enum OutputFormat {
        TRANSLATION, PATTERN, CUSTOM
    }

    @CommandLine.Option(names = {"-m", "--mapping"}, description = "Mappings for CUSTOM output format")
    private File mappingsFile;
    
    @CommandLine.Option(names = {"-v", "--version"}, description = "Minecraft version to compile for", arity = "1")
    private String versionString;
    HexGenerator.Versions version;

    @CommandLine.Option(names = {"-s", "--stacktrace"}, description = "Prints the stacktrace on error", defaultValue = "false")
    private boolean stackTrace;
    
    public static void main(String[] args) {
        int exitCode = new CommandLine(new Main()).execute(args);
        System.exit(exitCode);
    }
    
    @Override
    public Integer call() {
        if(listOptimizations) {
            System.out.println("Optimizations:");
            System.out.println(Optimizer.getAllOptimizations(null).stream().map(e -> "- " + e.getName()).collect(Collectors.joining("\n")));
            return SUCCESS;
        }
        
        if(versionString == null) {
            System.err.println("Please specify a compile version (-v)");
            return ARG_ERROR;
        }
        
        String vs = "V" + versionString.replaceAll("\\.", "_");
        try {
            version = HexGenerator.Versions.valueOf(vs);
        } catch (IllegalArgumentException e) {
            System.err.println("Unknown version: " + versionString);
            return ARG_ERROR;
        }
        
        return compileHexcFiles();
    }
    
    public int compileHexcFiles() {
        if(inputFiles.length >= 1) {
            List<Pair<String, List<Token>>> tokens = new ArrayList<>();
            try {
                for (File f : inputFiles) {
                    String src = InputFiles.readString(f);
                    tokens.add(Lexical.tokenize(f.getPath(), src.toCharArray()));
                }
            } catch (IOException e) {
                System.err.println("Error reading file: " + e.getMessage());
                if(stackTrace) {
                    System.err.println();
                    e.printStackTrace();
                }
                return IO_ERROR;
            } catch (TokenizationError e) {
                System.err.println(e.getMessage());
                if(stackTrace) {
                    System.err.println();
                    e.printStackTrace();
                }
                return TOKEN_ERROR;
            }

            NodePgrm pgrm;
            try {
                pgrm = Parser.parse(tokens);
            } catch (ParseError e) {
                System.err.println(e.getMessage());
                if(stackTrace) {
                    System.err.println();
                    e.printStackTrace();
                }
                return PARSE_ERROR;
            }

            IrHexCProgram irhexc;
            try {
                irhexc = IrHexCProgram.generate(pgrm);
            } catch (GeneratorError e) {
                System.err.println(e.getMessage());
                if(stackTrace) {
                    System.err.println();
                    e.printStackTrace();
                }
                return PARSE_ERROR;
            }
            
            if(optimizations == null || optimizations.length == 0) {
                optimizations = Optimizer.getAllOptimizations(null).stream().map(Optimization::getName).toArray(String[]::new);
            }
            Optimizer.runOptimizations(Arrays.stream(optimizations).toList(), null, irhexc, maxOptimizerPasses);

            List<String> hex = HexGenerator.generate(irhexc, version).stream().map(HexPattern::getHexString).collect(Collectors.toCollection(ArrayList::new));

            try {
                if (format == OutputFormat.PATTERN) {
                    PatternExporter.map(hex);
                } else if (format == OutputFormat.CUSTOM) {
                    if (mappingsFile == null) {
                        throw new RuntimeException("No mapping file specified (-m)");
                    } else {
                        CustomMappingExporter.map(hex, mappingsFile);
                    }
                }
            } catch (IOException e) {
                System.err.println("IO Error: " + e.getMessage());
                if(stackTrace) {
                    System.err.println();
                    e.printStackTrace();
                }
                return IO_ERROR;
            } catch (FormatterError e) {
                System.err.println(e.getMessage());
                if(stackTrace) {
                    System.err.println();
                    e.printStackTrace();
                }
                return IO_ERROR;
            }
            
            if (outputIrHexC) {
                try {
                    File out = new File("./" + outputFile + ".irhexc");
                    Files.write(out.toPath(), irhexc.getActions().stream().map(Object::toString).toList(), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
                } catch (IOException e) {
                    System.err.println("Write Error: " + e.getMessage());
                    if(stackTrace) {
                        System.err.println();
                        e.printStackTrace();
                    }
                    return IO_ERROR;
                }
            }

            try {
                File out = new File("./" + outputFile);
                Files.write(out.toPath(), hex, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
            } catch (IOException e) {
                System.err.println("Write Error: " + e.getMessage());
                if(stackTrace) {
                    System.err.println();
                    e.printStackTrace();
                }
                return IO_ERROR;
            }

            return SUCCESS;
        }
        System.err.println("No input files specified");
        return ARG_ERROR;
    }
}