package me.juliadev.jhexcc.codegen;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class GeneratorError extends RuntimeException {
    String msg;

    @Override
    public String getMessage() {
        return msg;
    }
}
