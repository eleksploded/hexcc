package me.juliadev.jhexcc.codegen.ir_hexc;

import lombok.*;
import me.juliadev.jhexcc.codegen.ir_hexc.scope_actions.*;
import me.juliadev.jhexcc.lex.tokens.TypeToken;
import me.juliadev.jhexcc.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@EqualsAndHashCode
public class Scope {
    final String scopeId;
    AtomicInteger nextAddr = new AtomicInteger(0);
    AtomicInteger stackDepth = new AtomicInteger(0);
    List<FunctionData> functions = new ArrayList<>();
    List<Vars> vars = new ArrayList<>();
    boolean returnable;

    @Getter
    Scope parentScope;

    public Scope(Scope parentScope, String id, boolean returnable) {
        this.parentScope = parentScope;
        this.returnable = returnable;
        if(parentScope != null) {
            this.scopeId = (!parentScope.scopeId.equals("root") ? (parentScope.scopeId + "-") : "") + id;
        } else {
            scopeId = "root";
        }
    }

    public Scope() {
        this(null, null, false);
    }

    public int getAddrSize() {
        return nextAddr.get();
    }

    public void push() {
        stackDepth.incrementAndGet();
    }

    public void pop(int count) {
        int depth = stackDepth.addAndGet(-count);
        if(depth < 0) {
            throw new RuntimeException("Stack underflow");
        }
    }

    public int getStackDepth() {
        return stackDepth.get();
    }

    public FunctionData makeFunction(String identifier, List<TypeToken.Types> params, TypeToken.Types returnType) {
        if(parentScope != null) throw new RuntimeException("Cannot create function in non-root scope");
        FunctionData func = new FunctionData(params, identifier, returnType, nextAddr.getAndIncrement());
        functions.add(func);
        return func;
    }

    public void makeVar(String identifier, TypeToken.Types type) {
        Vars var = new Vars(identifier, type, nextAddr.getAndIncrement());
        vars.add(var);
    }

    public IrHexCSaveReturnJump makeReturnJumpSave() {
        if(!vars.isEmpty()) throw new RuntimeException("Return jump must be addr 0");
        Vars var = new Vars("$returnJump$", TypeToken.Types.VOID, nextAddr.getAndIncrement());
        vars.add(var);
        return new IrHexCSaveReturnJump(1, 0, "$returnJump$", TypeToken.Types.VOID);
    }

    public IrHexCReturn makeReturn() {
        Pair<Integer, Vars> scopeSearch = search("$returnJump$");
        return new IrHexCReturn(scopeSearch.getLeft());
    }

    @AllArgsConstructor(access = AccessLevel.PRIVATE) @Getter @ToString
    public static class FunctionData {
        List<TypeToken.Types> paramTypes;
        String identifier;
        TypeToken.Types returnType;

        int addr;
    }

    @AllArgsConstructor(access = AccessLevel.PRIVATE) @ToString
    private static class Vars {
        String identifier;
        TypeToken.Types type;

        int addr;
    }

    public FunctionData findFunc(String identifier, List<TypeToken.Types> params) {
        for (FunctionData function : functions) {
            if(function.identifier.equals(identifier) && function.paramTypes.equals(params)) {
                return function;
            }
        }

        throw new RuntimeException("function " + identifier + " not found");
    }

    public IrHexCOpenScope openChildScope(String id, boolean isReturnable) {
        Scope scope = new Scope(this, id, isReturnable);
        return new IrHexCOpenScope(scope, isReturnable);
    }

    public IrHexCCloseScope closeScope() {
        return new IrHexCCloseScope(returnable);
    }

    public IrHexCIdentifierGet readVar(String identifier) {
        Pair<Integer, Vars> scopeSearch = search(identifier);
        return new IrHexCIdentifierGet(scopeSearch.getLeft(), scopeSearch.getRight().addr, identifier, scopeSearch.getRight().type);
    }

    public IrHexCIdentifierSet writeVar(String identifier) {
        Pair<Integer, Vars> scopeSearch = search(identifier);
        return new IrHexCIdentifierSet(scopeSearch.getLeft(), scopeSearch.getRight().addr, identifier, scopeSearch.getRight().type);
    }

    public IrHexCParamWrite writeParam(String identifier, TypeToken.Types paramType, int paramIndex) {
        Pair<Integer, Vars> scopeSearch = search(identifier);
        return new IrHexCParamWrite(scopeSearch.getLeft(), scopeSearch.getRight().addr, paramIndex, identifier, paramType);
    }

    private Pair<Integer, Vars> search(String ident) {
        Scope targetScope = this;
        Vars target = null;
        int stackDepth = getStackDepth();

        for (Vars var : vars) {
            if(var.identifier.equals(ident)) {
                target = var;
            }
        }

        while(target == null) {
            targetScope = targetScope.getParentScope();
            if(targetScope == null) {
                throw new RuntimeException("identifier " + ident + " not found");
            }
            stackDepth += targetScope.getStackDepth();

            for (Vars var : targetScope.vars) {
                if(var.identifier.equals(ident)) {
                    target = var;
                    break;
                }
            }
        }

        return Pair.of(stackDepth, target);
    }

    public int traceToRoot() {
        if(getParentScope() == null) {
            return getStackDepth();
        } else {
            return getParentScope().traceToRoot() + getStackDepth();
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Scope (");
        sb.append(this.scopeId);
        sb.append(") [\n");

        if(!functions.isEmpty()) {
            sb.append("  Functions:\n");
            for(FunctionData func : functions) {
                sb.append("    ");
                sb.append(func.toString());
                sb.append("\n");
            }
        }
        if(!vars.isEmpty()) {
            sb.append("  Vars:\n");
            for(Vars var : vars) {
                sb.append("    ");
                sb.append(var.toString());
                sb.append("\n");
            }
        }
        sb.append("  Address Size: ").append(getAddrSize()).append("\n");
        sb.append("  Stack Depth: ").append(getStackDepth()).append("\n");
        sb.append("  Returnable: ").append(returnable).append("\n");
        sb.append("]");
        return sb.toString();
    }
}
