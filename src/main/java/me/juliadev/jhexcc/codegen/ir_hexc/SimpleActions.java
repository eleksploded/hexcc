package me.juliadev.jhexcc.codegen.ir_hexc;

public enum SimpleActions implements IrHexCAction {
    LIST_DECOMPOSE, DUP, DROP, IF, FOR, LIST_SELECT,
    VEC_REFL_X, VEC_REFL_MX, VEC_REFL_Y, VEC_REFL_MY, VEC_REFL_Z, VEC_REFL_MZ,
    
    ADD, SUB, MUL, DIV, GT, LT, GTE, LTE, MAKE_VEC, EQU, N_EQU, NOT;
    //TODO: rest of the operators


    @Override
    public String toString() {
        return this.name();
    }
}
