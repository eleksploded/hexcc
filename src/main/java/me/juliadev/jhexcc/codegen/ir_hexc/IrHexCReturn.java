package me.juliadev.jhexcc.codegen.ir_hexc;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter @AllArgsConstructor @ToString
public class IrHexCReturn implements IrHexCAction {
    int scopeDepth;
}
