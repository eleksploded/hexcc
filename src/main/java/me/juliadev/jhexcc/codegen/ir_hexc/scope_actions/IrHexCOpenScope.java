package me.juliadev.jhexcc.codegen.ir_hexc.scope_actions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import me.juliadev.jhexcc.codegen.ir_hexc.IrHexCAction;
import me.juliadev.jhexcc.codegen.ir_hexc.Scope;

@AllArgsConstructor @Getter @ToString
public class IrHexCOpenScope implements IrHexCAction {
    Scope scope;

    boolean returnable = false;
}
