package me.juliadev.jhexcc.codegen.ir_hexc.scope_actions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import me.juliadev.jhexcc.codegen.ir_hexc.IrHexCAction;
import me.juliadev.jhexcc.codegen.ir_hexc.Scope;
import me.juliadev.jhexcc.lex.tokens.TypeToken;

@AllArgsConstructor @Getter @ToString
public class IrHexCIdentifierGet implements IrHexCAction {
    int scopeDepth;
    int inScopeAddr;

    String identifier;
    TypeToken.Types type;

    public static IrHexCIdentifierGet getFunction(Scope.FunctionData func) {
        return new IrHexCIdentifierGet(-1, func.getAddr(), func.getIdentifier(), func.getReturnType());
    }
}
