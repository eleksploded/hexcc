package me.juliadev.jhexcc.codegen.ir_hexc;

import lombok.Getter;
import lombok.Setter;
import me.juliadev.jhexcc.codegen.hex.Bookkeepers;
import me.juliadev.jhexcc.codegen.ir_hexc.scope_actions.IrHexCCreateRootScope;
import me.juliadev.jhexcc.codegen.ir_hexc.scope_actions.IrHexCIdentifierGet;
import me.juliadev.jhexcc.codegen.ir_hexc.scope_actions.IrHexCIdentifierSet;
import me.juliadev.jhexcc.codegen.ir_hexc.scope_actions.IrHexCOpenScope;
import me.juliadev.jhexcc.lex.tokens.Special;
import me.juliadev.jhexcc.lex.tokens.TypeToken;
import me.juliadev.jhexcc.parse.nodes.*;
import me.juliadev.jhexcc.util.Pair;

import java.util.*;

public class IrHexCProgram {
    private static final Map<Special, Map<Pair<TypeToken.Types, TypeToken.Types>, TypeToken.Types>> operTypeLookup;
    static {
        Map<Special, Map<Pair<TypeToken.Types, TypeToken.Types>, TypeToken.Types>> map = new HashMap<>();
        for(Special op : List.of(Special.PLUS, Special.SUB, Special.STAR, Special.DIV)) {
            addOperatorTypeMapping(map, op, TypeToken.Types.NUM, TypeToken.Types.NUM, TypeToken.Types.NUM);
            addOperatorTypeMapping(map, op, TypeToken.Types.VEC, TypeToken.Types.VEC, TypeToken.Types.VEC);
            addOperatorTypeMapping(map, op, TypeToken.Types.NUM, TypeToken.Types.VEC, TypeToken.Types.VEC);
            addOperatorTypeMapping(map, op, TypeToken.Types.VEC, TypeToken.Types.NUM, TypeToken.Types.VEC);
        }
        addOperatorTypeMapping(map, Special.GT, TypeToken.Types.NUM, TypeToken.Types.NUM, TypeToken.Types.BOOL);
        addOperatorTypeMapping(map, Special.LT, TypeToken.Types.NUM, TypeToken.Types.NUM, TypeToken.Types.BOOL);
        addOperatorTypeMapping(map, Special.GTE, TypeToken.Types.NUM, TypeToken.Types.NUM, TypeToken.Types.BOOL);
        addOperatorTypeMapping(map, Special.LTE, TypeToken.Types.NUM, TypeToken.Types.NUM, TypeToken.Types.BOOL);
        
        operTypeLookup = Collections.unmodifiableMap(map);
    }
    private static void addOperatorTypeMapping(Map<Special, Map<Pair<TypeToken.Types, TypeToken.Types>, TypeToken.Types>> map, Special operator, TypeToken.Types lhs, TypeToken.Types rhs, TypeToken.Types out) {
        Map<Pair<TypeToken.Types, TypeToken.Types>, TypeToken.Types> operMap = map.getOrDefault(operator, new HashMap<>());
        operMap.put(lhs.ordinal() > rhs.ordinal() ? Pair.of(rhs, lhs) : Pair.of(lhs, rhs), out);
        map.put(operator, operMap);
    }

    public static IrHexCProgram generate(NodePgrm pgrm) {
        IrHexCProgram generator = new IrHexCProgram();
        generator.visitNode(pgrm);
        return generator;
    }

    @Getter @Setter
    private Scope rootScope;
    private Scope currentScope;
    @Getter @Setter
    private List<IrHexCAction> actions = new ArrayList<>();
    @Getter @Setter
    private int optimizationPasses = 0;

    private TypeToken.Types lastStmtReturnType = null;
    private TypeToken.Types lastExprType = null;

    private IrHexCProgram() {}
    
    @Override
    public int hashCode() {
        return Objects.hash(actions, rootScope);
    }
    
    private void visit(Object object) {
        if(object instanceof NodePgrm v) visitNode(v);
        else if(object instanceof NodeFuncDec v) visitNode(v);
        else if(object instanceof NodeVarDec v) visitNode(v);
        else if(object instanceof NodeStmtList v) visitNode(v);
        else if(object instanceof NodeFuncCall v) visitNode(v);
        else if(object instanceof NodeReturn v) visitNode(v);
        else if(object instanceof NodeIdent v) visitNode(v);
        else if(object instanceof NodeOperExpr v) visitNode(v);
        else if(object instanceof NodeLiteral v) visitNode(v);
        else if(object instanceof NodeRawStmt v) visitNode(v);
        else if(object instanceof NodeConditional v) visitNode(v);
        else if(object instanceof NodeVarSet v) visitNode(v);
        else if(object instanceof NodeIter v) visitNode(v);
        else if(object instanceof NodeList v) visitNode(v);
        else if(object instanceof NodeVector v) visitNode(v);
        else if(object instanceof NodeIndex v) visitNode(v);

        else {
            throw new RuntimeException("dont know how to visit " + object.getClass().getSimpleName());
        }
    }
    
    private void visitNode(NodeIndex v) {
        visit(v.getIdent());
        visit(v.getIndex());
        if(lastExprType != TypeToken.Types.NUM) {
            throw new RuntimeException("index must be a number");
        }
        actions.add(SimpleActions.LIST_SELECT);
        currentScope.pop(2);
        currentScope.push();

        lastExprType = v.getType();
    }
    
    private void visitNode(NodeList v) {
        for(NodeExpr n : v.getValues()) visit(n);
        actions.add(new IrHexCListBuild(v.getValues().size()));
        currentScope.pop(v.getValues().size());
        currentScope.push();

        lastExprType = TypeToken.Types.LIST;
    }

    private void visitNode(NodeVector v) {
        visit(v.getX());
        if(lastExprType != TypeToken.Types.NUM) throw new RuntimeException("type error, vector got " + lastExprType.name() + " instead of NUM");
        visit(v.getY());
        if(lastExprType != TypeToken.Types.NUM) throw new RuntimeException("type error, vector got " + lastExprType.name() + " instead of NUM");
        visit(v.getZ());
        if(lastExprType != TypeToken.Types.NUM) throw new RuntimeException("type error, vector got " + lastExprType.name() + " instead of NUM");
        actions.add(SimpleActions.MAKE_VEC);
        lastExprType = TypeToken.Types.VEC;

        currentScope.pop(3);
        currentScope.push();
    }

    private void visitNode(NodeIter v) { //TODO: Stack counting, changes will not persist
        openScope("iter", false);
        currentScope.makeVar(v.getIdent().getIdentifier(), v.getType().getType());
        actions.add(currentScope.writeVar(v.getIdent().getIdentifier()));
        visit(v.getStmts());
        closeScope();
        visit(v.getExpr());
        actions.add(SimpleActions.FOR);
    }
    
    private void visitNode(NodeVarSet varSet) {
        visit(varSet.getValue());
        IrHexCIdentifierSet idSet = currentScope.writeVar(varSet.getIdent().getIdentifier());
        if(lastExprType != idSet.getType()) {
            throw new RuntimeException("tried to assign " + lastExprType + " to a variable of type " + idSet.getType().name());
        }
        actions.add(idSet);
        currentScope.pop(1);
    }
    
    private void visitNode(NodeConditional cond) {
        visit(cond.getCondition());
        currentScope.pop(1);

        openScope("ifTrue", false);
        visit(cond.getIfTrue());
        closeScope();

        if(cond.getIfFalse() != null) {
            openScope("ifFalse", false);
            visit(cond.getIfFalse());
            closeScope();
        } else {
            actions.add(new IrHexCHexEmbed(new Bookkeepers("-").getHexString()));
        }
        actions.add(SimpleActions.IF);
        actions.add(new IrHexCExec(false));
    }
    
    private void visitNode(NodeRawStmt v) {
        for (Pair<NodeRawStmt.Instruction, String> inst : v.getInstructions()) {
            switch (inst.getLeft()) {
                case HEX -> actions.add(new IrHexCRawHex(inst.getRight()));
                case EMBED -> actions.add(new IrHexCHexEmbed(inst.getRight()));
                case READ -> actions.add(currentScope.readVar(inst.getRight()));
                case WRITE -> actions.add(currentScope.writeVar(inst.getRight()));
            }
        }
    }
    
    private void visitNode(NodeLiteral v) {
        if(v.getType().equals(Integer.class)) {
            actions.add(new IrHexCLiteral(v.getValue(), TypeToken.Types.NUM));
            lastExprType = TypeToken.Types.NUM;
            currentScope.push();
        } else {
            throw new RuntimeException("unable to visit " + v.getType().getSimpleName() + " literals");
        }
    }

    private void visitNode(NodeOperExpr v) {
        visit(v.getLeft());
        TypeToken.Types lhsType = lastExprType;
        visit(v.getRight());
        TypeToken.Types rhsType = lastExprType;

        Pair<TypeToken.Types, TypeToken.Types> operTypes =
                lhsType.ordinal() > rhsType.ordinal() ?
                        Pair.of(rhsType, lhsType) : Pair.of(lhsType, rhsType);

        switch (v.getOp().operator()) {
            case PLUS -> actions.add(SimpleActions.ADD);
            case SUB -> actions.add(SimpleActions.SUB);
            case STAR -> actions.add(SimpleActions.MUL);
            case DIV -> actions.add(SimpleActions.DIV);
            case GT -> actions.add(SimpleActions.GT);
            case LT -> actions.add(SimpleActions.LT);
            case GTE -> actions.add(SimpleActions.GTE);
            case LTE -> actions.add(SimpleActions.LTE);
            case EQU -> actions.add(SimpleActions.EQU);
            case NOT_EQU -> actions.add(SimpleActions.N_EQU);
            case NOT -> actions.add(SimpleActions.NOT);
            default -> throw new RuntimeException("unknown operator " + v.getOp().operator().name());
        }

        if(v.getOp().operator() == Special.NOT_EQU || v.getOp().operator() == Special.EQU) {
            //dont check type cause equ and not equ dont care, they always give a bool
            lastExprType = TypeToken.Types.BOOL;
        } else {
            Map<Pair<TypeToken.Types, TypeToken.Types>, TypeToken.Types> validTypePairs = operTypeLookup.get(v.getOp().operator());
            if (!validTypePairs.containsKey(operTypes))
                throw new RuntimeException("operator " + v.getOp().operator() + " does not support " + operTypes.getKey() + " and " + operTypes.getValue());
            lastExprType = validTypePairs.get(operTypes);
        }

        currentScope.pop(2);
        currentScope.push();
    }

    private void visitNode(NodeIdent v) {
        IrHexCIdentifierGet read = currentScope.readVar(v.getIdentifier());
        actions.add(read);
        lastExprType = read.getType();
        currentScope.push();
    }

    private void visitNode(NodeReturn v) { //TODO: Stack counting
        if(currentScope.getStackDepth() > 0) {
            for(int i = 0; i < currentScope.getStackDepth(); i++) {
                actions.add(SimpleActions.DROP);
            }
            currentScope.pop(currentScope.getStackDepth());
        }

        visit(v.getExpr());
        lastStmtReturnType = lastExprType;
        actions.add(currentScope.makeReturn());
    }

    private void visitNode(NodeFuncCall v) {
        List<TypeToken.Types> paramTypes = new ArrayList<>();
        for (NodeExpr param : v.getParams()) {
            visit(param);
            paramTypes.add(lastExprType);
        }
        
        Scope.FunctionData func = rootScope.findFunc(v.getIdent().getIdentifier(), paramTypes);
        actions.add(IrHexCIdentifierGet.getFunction(func));
        actions.add(new IrHexCExec(true));
        lastExprType = func.getReturnType();
        currentScope.pop(v.getParams().size());
        if(lastExprType != TypeToken.Types.VOID) {
            currentScope.push();
        }
    }

    private void visitNode(NodeStmtList v) {
        for (NodeStmtList.NodeStmtParam stmt : v.getStmts()) {
            visit(stmt);

            if(stmt instanceof NodeFuncCall) {
                if(lastExprType != TypeToken.Types.VOID) {
                    actions.add(SimpleActions.DROP);
                    currentScope.pop(1);
                }
            }
        }
    }

    private void visitNode(NodeVarDec varDec) {
        currentScope.makeVar(varDec.getIdent().getIdentifier(), varDec.getType().getType());

        if(varDec.getExpr() != null) {
            visit(NodeVarSet.makeDummy(varDec. getIdent(), varDec.getExpr()));
        }
    }

    private void visitNode(NodeFuncDec funcDec) {
        Scope.FunctionData function = currentScope.makeFunction(funcDec.getIdent().getIdentifier(), funcDec.getParams().stream().map(p -> p.getType().getType()).toList(), funcDec.getType().getType());

        openScope(function.getIdentifier(), true);
        actions.add(currentScope.makeReturnJumpSave());

        lastStmtReturnType = TypeToken.Types.VOID;

        List<IrHexCAction> writes = new ArrayList<>();
        for (int i = 0; i < funcDec.getParams().size(); i++) {
            NodeParam param = funcDec.getParams().get(i);
            currentScope.makeVar(param.getIdent().getIdentifier(), param.getType().getType());
            writes.add(currentScope.writeParam(param.getIdent().getIdentifier(), param.getType().getType(), funcDec.getParams().size() - i));
        }
        Collections.reverse(writes);
        actions.addAll(writes);

        visit(funcDec.getStmts());
        if(lastStmtReturnType == null && funcDec.getType().getType() != TypeToken.Types.VOID) {
            throw new RuntimeException("No return statement in nonvoid function " + funcDec.getIdent().getIdentifier());
        } else if (!Objects.equals(lastStmtReturnType, funcDec.getType().getType())) {
            throw new RuntimeException("Function %s returns %s but got %s".formatted(funcDec.getIdent().getIdentifier(), funcDec.getType().getType(), lastStmtReturnType == null ? "nothing" : lastStmtReturnType));
        }
        
        closeScope();
        currentScope.push();

        actions.add(IrHexCIdentifierSet.setFunction(function));
        currentScope.pop(1);
    }

    public void visitNode(NodePgrm pgrm) {
        rootScope = new Scope();
        currentScope = rootScope;
        actions.add(new IrHexCCreateRootScope(rootScope));

        for (NodeDec nodeDec : pgrm.getDec()) {
            visit(nodeDec.getParam());
        }

        Scope.FunctionData main = rootScope.findFunc("main", List.of());
        if(main.getReturnType() != TypeToken.Types.VOID) throw new RuntimeException("Main should return void");

        actions.add(IrHexCIdentifierGet.getFunction(main));
        currentScope.push();
        actions.add(new IrHexCExec(true));
    }

    private void closeScope() {
        actions.add(currentScope.closeScope());
        currentScope = currentScope.getParentScope();
        currentScope.pop(1);
    }

    private void openScope(String id, boolean returnable) {
        IrHexCOpenScope newScope = currentScope.openChildScope(id, returnable);
        actions.add(newScope);
        currentScope.push();
        currentScope = newScope.getScope();
    }
}
