package me.juliadev.jhexcc.codegen.ir_hexc.scope_actions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import me.juliadev.jhexcc.codegen.ir_hexc.IrHexCAction;
import me.juliadev.jhexcc.lex.tokens.TypeToken;

@AllArgsConstructor @Getter @ToString
public class IrHexCParamWrite implements IrHexCAction {
    int scopeDepth;
    int inScopeAddr;
    int paramDepth;

    String identifier;
    TypeToken.Types type;
}
