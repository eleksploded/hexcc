package me.juliadev.jhexcc.codegen.ir_hexc;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import me.juliadev.jhexcc.lex.tokens.TypeToken;

@Getter
@AllArgsConstructor
@ToString
public class IrHexCLiteral implements IrHexCAction {
    Object value;
    TypeToken.Types type;
}
