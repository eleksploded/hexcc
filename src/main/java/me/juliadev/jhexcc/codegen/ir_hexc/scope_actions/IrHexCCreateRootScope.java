package me.juliadev.jhexcc.codegen.ir_hexc.scope_actions;

import lombok.ToString;
import me.juliadev.jhexcc.codegen.ir_hexc.Scope;

@ToString(callSuper = true)
public class IrHexCCreateRootScope extends IrHexCOpenScope {
    public IrHexCCreateRootScope(Scope scope) {
        super(scope, false);
    }
}
