package me.juliadev.jhexcc.codegen.ir_hexc;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public class IrHexCRawHex implements IrHexCAction {
    String hex;
}
