package me.juliadev.jhexcc.codegen.ir_hexc;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor @Getter @ToString
public class IrHexCExec implements IrHexCAction {
    boolean returnable;
}
