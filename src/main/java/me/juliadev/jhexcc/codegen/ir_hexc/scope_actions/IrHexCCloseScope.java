package me.juliadev.jhexcc.codegen.ir_hexc.scope_actions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import me.juliadev.jhexcc.codegen.ir_hexc.IrHexCAction;

@AllArgsConstructor @Getter @ToString
public class IrHexCCloseScope implements IrHexCAction {
    boolean returnable;
}
