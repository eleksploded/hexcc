package me.juliadev.jhexcc.codegen.hex;

import lombok.Getter;

@Getter
public enum Patterns implements HexPattern {
    GEMINI_DECOMP("Gemini Decomposition"),
    RAVEN_LOAD("Muninn's Reflection"),
    RAVEN_SAVE("Huginn's Gambit"),
    HERMES("Hermes' Gambit"),
    IRIS("Iris' Gambit"),
    SURGEON_EXALT("Surgeon's Exaltation"),
    SELECT_DIST("Selection Distillation"),
    VACANT_REFL("Vacant Reflection"),
    ADD_DIST("Additive Distillation"),
    SUB_DIST("Subtractive Distillation"),
    MUL_DIST("Multiplicative Distillation"),
    DIV_DIST("Division Distillation"),
    FLOCK_DIS("Flock's Disintegration"),
    FLOCK_GAMBIT("Flock's Gambit"),
    INTRO("Introspection"),
    RETRO("Retrospection"),
    GT("Maximus Distillation"),
    LT("Minimus Distillation"),
    GTE("Maximus Distillation II"),
    LTE("Minimus Distillation II"),
    TO_BOOL("Augur's Purification"),
    SELECT_EXALT("Selection Exaltation"),
    IF("Augur's Exaltation"),
    FOR("Thoth's Gambit"),
    
    VEC_REFL_X("Vector Reflection +X"),
    VEC_REFL_MX("Vector Reflection -X"),
    VEC_REFL_Y("Vector Reflection +Y"),
    VEC_REFL_MY("Vector Reflection -Y"),
    VEC_REFL_Z("Vector Reflection +Z"),
    VEC_REFL_MZ("Vector Reflection -Z"),
    VEC_EXALT("Vector Exaltation"),
    ROT("Rotation Gambit"),
    NULL("Nullary Reflection"),
    DUP_N("Gemini Gambit"),
    SINGLE_LIST("Single's Purification"),
    EQUAL("Equality Distillation"),
    NOT_EQUAL("Inequality Distillation"),
    GET_N("Fisherman's Gambit"),
    COPY_N("Fisherman's Gambit II"),
    CHARON("Charon's Gambit"),
    LIST_REVERSE("Retrograde Purification"),
    LIST_GET_HEAD("Speaker's Decomposition"),
    STACK_LENGTH("Flock's Reflection"),
    JESTER("Jester's Gambit");

    final String hex;
    Patterns(String hex) {
        this.hex = hex;
    }

    @Override
    public String getHexString() {
        return hex;
    }
}
