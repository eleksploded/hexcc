package me.juliadev.jhexcc.codegen.hex;

public interface HexPattern {
    String getHexString();
}
