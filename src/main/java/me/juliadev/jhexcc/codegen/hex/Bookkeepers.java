package me.juliadev.jhexcc.codegen.hex;

public class Bookkeepers implements HexPattern {
    private final String pattern;
    public Bookkeepers(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public String getHexString() {
        return "Bookkeeper's Gambit: " + pattern;
    }
}
