package me.juliadev.jhexcc.codegen.hex;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class RawHexPattern implements HexPattern {
    private final String hexString;
}
