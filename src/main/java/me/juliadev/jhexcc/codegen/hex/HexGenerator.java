package me.juliadev.jhexcc.codegen.hex;

import me.juliadev.jhexcc.codegen.hex.generators.HexGenerator119;
import me.juliadev.jhexcc.codegen.hex.generators.HexGenerator1201;
import me.juliadev.jhexcc.codegen.ir_hexc.IrHexCProgram;

import java.util.List;
import java.util.function.Function;

public class HexGenerator {
    public enum Versions {
        V1_19_2(HexGenerator119::generate),
        V1_20_1(HexGenerator1201::generate);

        final Function<IrHexCProgram, List<HexPattern>> genFunction;
        Versions(Function<IrHexCProgram, List<HexPattern>> genFunction) {
            this.genFunction = genFunction;
        }
    }

    public static Versions version;
    public static List<HexPattern> generate(IrHexCProgram irHexCProgram, Versions versions) {
        version = versions;
        return versions.genFunction.apply(irHexCProgram);
    }
}
