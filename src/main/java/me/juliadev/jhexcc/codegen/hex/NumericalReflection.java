package me.juliadev.jhexcc.codegen.hex;

public class NumericalReflection implements HexPattern {
    final boolean whole;
    final float value;

    public NumericalReflection(int value) {
        this.value = value;
        whole = true;
    }

    public NumericalReflection(float value) {
        this.value = value;
        whole = false;
    }

    @Override
    public String getHexString() {
        return String.format("Numerical Reflection: " + (whole ? "%.0f" : "%f"), value);
    }
}
