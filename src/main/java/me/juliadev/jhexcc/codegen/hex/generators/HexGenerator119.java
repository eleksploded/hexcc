package me.juliadev.jhexcc.codegen.hex.generators;

import me.juliadev.jhexcc.codegen.GeneratorError;
import me.juliadev.jhexcc.codegen.hex.*;
import me.juliadev.jhexcc.codegen.ir_hexc.*;
import me.juliadev.jhexcc.codegen.ir_hexc.scope_actions.*;

import java.util.ArrayList;
import java.util.List;

public class HexGenerator119 {
    public static List<HexPattern> generate(IrHexCProgram ir) {
        List<HexPattern> hex = new ArrayList<>();
        for (IrHexCAction a : ir.getActions()) {
            if(hex.size() > 1 && hex.get(hex.size() - 1) == Patterns.CHARON) {
                if(!(a instanceof IrHexCCloseScope)) {
                    throw new GeneratorError("Return must be the last action in a function");
                }
            }

            if(a instanceof IrHexCIdentifierSet action) visitAction(hex, action);
            else if(a instanceof IrHexCCreateRootScope list) visitAction(hex, list);
            else if(a instanceof IrHexCIdentifierGet action) visitAction(hex, action);
            else if(a instanceof SimpleActions action) visitAction(hex, action);
            else if(a instanceof IrHexCHexEmbed action) visitAction(hex, action);
            else if(a instanceof IrHexCOpenScope action) visitAction(hex, action);
            else if(a instanceof IrHexCCloseScope s) visitAction(hex, s);
            else if(a instanceof IrHexCLiteral lit) visitLiteral(hex, lit);
            else if(a instanceof IrHexCRawHex raw) hex.add(new RawHexPattern(raw.getHex()));
            else if(a instanceof IrHexCListBuild list) visitAction(hex, list);
            else if(a instanceof IrHexCSaveReturnJump list) {}
            else if(a instanceof IrHexCReturn ret) visitAction(hex, ret);
            else if(a instanceof IrHexCParamWrite w) visitAction(hex, w);
            else if(a instanceof IrHexCExec w) visitAction(hex, w);

            else throw new GeneratorError("unable to visit " + a);
        }

        return hex;
    }

    private static void visitAction(List<HexPattern> hex, IrHexCReturn ret) {
        hex.add(new Bookkeepers("v".repeat(ret.getScopeDepth()) + "-"));
    }

    private static void visitAction(List<HexPattern> hex, IrHexCCloseScope cScope) {
        hex.add(new Bookkeepers("v"));
        hex.add(Patterns.RETRO);
    }

    private static void visitAction(List<HexPattern> hex, IrHexCExec list) {
        hex.add(Patterns.HERMES);
    }

    private static void visitAction(List<HexPattern> hex, IrHexCParamWrite w) {
        hex.add(new NumericalReflection(w.getInScopeAddr()));
        hex.add(new NumericalReflection(w.getParamDepth() + 1));
        hex.add(Patterns.GET_N);
        hex.add(Patterns.SURGEON_EXALT);
    }

    private static void visitAction(List<HexPattern> hex, IrHexCOpenScope action) {
        hex.add(Patterns.INTRO);
        initMem(hex, action.getScope().getAddrSize());
    }

    private static void visitAction(List<HexPattern> hex, IrHexCCreateRootScope init) {
        initMem(hex, init.getScope().getAddrSize());
    }

    private static void initMem(List<HexPattern> hex, int size) {
        if(size == 0) {
            hex.add(Patterns.VACANT_REFL);
        } else if (size == 1) {
            hex.add(Patterns.NULL);
            hex.add(Patterns.SINGLE_LIST);
        } else {
            hex.add(Patterns.NULL);
            hex.add(new NumericalReflection(size));
            hex.add(Patterns.DUP_N);
            hex.add(new NumericalReflection(size));
            hex.add(Patterns.FLOCK_GAMBIT);
        }
    }

    private static void visitAction(List<HexPattern> hex, IrHexCListBuild list) {
        if(list.getLength() == 1) {
            hex.add(Patterns.SINGLE_LIST);
        } else {
            hex.add(new NumericalReflection(list.getLength()));
            hex.add(Patterns.FLOCK_GAMBIT);
        }
    }

    private static void visitAction(List<HexPattern> hex, IrHexCHexEmbed action) {
        hex.add(Patterns.INTRO);
        hex.add(new RawHexPattern(action.getHex()));
        hex.add(Patterns.RETRO);
        hex.add(Patterns.FLOCK_DIS);
    }
    
    private static void visitLiteral(List<HexPattern> hex, IrHexCLiteral lit) {
        switch (lit.getType()) {
            case VOID -> throw new IllegalStateException();
            case NUM -> hex.add(new NumericalReflection(((Integer) lit.getValue())));
            case ENT -> throw new AssertionError("ent not implemented");
            case PAT -> throw new AssertionError("pat not implemented");
            default -> throw new GeneratorError("Type not a literal");
        }
    }

    private static void visitAction(List<HexPattern> hex, SimpleActions action) {

        switch (action) {
            case LIST_DECOMPOSE -> hex.add(Patterns.FLOCK_DIS);
            case DUP -> hex.add(Patterns.GEMINI_DECOMP);
            case DROP -> hex.add(new Bookkeepers("v"));
            case ADD -> hex.add(Patterns.ADD_DIST);
            case SUB -> hex.add(Patterns.SUB_DIST);
            case MUL -> hex.add(Patterns.MUL_DIST);
            case DIV -> hex.add(Patterns.DIV_DIST);
            case GT -> {
                hex.add(Patterns.GT);
                hex.add(Patterns.TO_BOOL);
            }
            case LT -> {
                hex.add(Patterns.LT);
                hex.add(Patterns.TO_BOOL);
            }
            case GTE -> {
                hex.add(Patterns.GTE);
                hex.add(Patterns.TO_BOOL);
            }
            case LTE -> {
                hex.add(Patterns.LTE);
                hex.add(Patterns.TO_BOOL);
            }
            case EQU -> {
                hex.add(Patterns.EQUAL);
                hex.add(Patterns.TO_BOOL);
            }
            case N_EQU -> {
                hex.add(Patterns.NOT_EQUAL);
                hex.add(Patterns.TO_BOOL);
            }
            case IF -> hex.add(Patterns.IF);
            case FOR -> hex.add(Patterns.FOR);
            case VEC_REFL_X -> hex.add(Patterns.VEC_REFL_X);
            case VEC_REFL_MX -> hex.add(Patterns.VEC_REFL_MX);
            case VEC_REFL_Y -> hex.add(Patterns.VEC_REFL_Y);
            case VEC_REFL_MY -> hex.add(Patterns.VEC_REFL_MY);
            case VEC_REFL_Z -> hex.add(Patterns.VEC_REFL_Z);
            case VEC_REFL_MZ -> hex.add(Patterns.VEC_REFL_MZ);
            case MAKE_VEC -> hex.add(Patterns.VEC_EXALT);
            case LIST_SELECT -> hex.add(Patterns.SELECT_DIST);
            default -> throw new GeneratorError("can't process simple action: " + action);
        }
    }

    private static void visitAction(List<HexPattern> hex, IrHexCIdentifierGet action) {
        hex.add(new NumericalReflection(action.getScopeDepth()));
        hex.add(Patterns.COPY_N);
        hex.add(new NumericalReflection(action.getInScopeAddr()));
        hex.add(Patterns.SELECT_DIST);
    }

    private static void visitAction(List<HexPattern> hex, IrHexCIdentifierSet action) {
        hex.add(new NumericalReflection(action.getScopeDepth()));
        hex.add(Patterns.GET_N);
        hex.add(new NumericalReflection(action.getInScopeAddr()));
        hex.add(Patterns.ROT);
        hex.add(Patterns.SURGEON_EXALT);

        hex.add(Patterns.STACK_LENGTH);
        hex.add(Patterns.FLOCK_GAMBIT);
        hex.add(Patterns.LIST_REVERSE);
        hex.add(Patterns.LIST_GET_HEAD);
        hex.add(new NumericalReflection(action.getScopeDepth()));
        hex.add(Patterns.JESTER);
        hex.add(Patterns.SURGEON_EXALT);
        hex.add(Patterns.FLOCK_DIS);
    }
}
