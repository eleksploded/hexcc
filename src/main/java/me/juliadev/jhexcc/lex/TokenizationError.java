package me.juliadev.jhexcc.lex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.juliadev.jhexcc.HexCCError;

@AllArgsConstructor
@Getter
public class TokenizationError extends HexCCError {

    String error;
    String fileName;
    int line;
    int column;

    @Override
    public String getMessage() {
        return getMessage(fileName, error, line, column);
    }
}
