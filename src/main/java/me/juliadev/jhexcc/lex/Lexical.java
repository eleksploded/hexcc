package me.juliadev.jhexcc.lex;

import me.juliadev.jhexcc.lex.tokens.*;
import me.juliadev.jhexcc.util.Pair;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.regex.Pattern;

public class Lexical {
    public static final Map<String, BiFunction<Integer, Integer, Token>> keywords = new HashMap<>();
    public static final Map<String, BiFunction<Integer, Integer, Token>> specialTokens = new HashMap<>();

    private static final Predicate<String> isValidRawCharacter = Pattern.compile("[a-zA-Z0-9:#$/()' ]").asMatchPredicate();
    
    public static Pair<String, List<Token>> tokenize(String fName, char[] src) {
        Keywords.values();
        Special.values();

        List<Token> tokens = new LinkedList<>();
        
        StringBuffer buf = new StringBuffer();
        
        int charIndex = 0;
        int currentLine = 0, lineStartIndex = 0;

        while (charIndex < src.length) {
            if(shouldParseRaw(tokens)) {
                clear(buf);
                while(true) {
                    char c = src[charIndex++];
                    
                    if(c == '}') {
                        tokens.add(Special.CLOSE_BRACE.makeToken(currentLine, (charIndex - 1) - lineStartIndex));
                        clear(buf);
                        break;
                    } else if(c == '\n') {
                        if(!buf.toString().trim().isEmpty()) tokens.add(new RawEntryToken(buf.toString().trim(), currentLine, (charIndex - 1) - lineStartIndex - buf.length()));
                        clear(buf);
                        currentLine++;
                        lineStartIndex = charIndex;
                    } else {
                        if(isValidRawCharacter.test(String.valueOf(c))) {
                            buf.append(c);
                        } else {
                            throw new TokenizationError("Invalid Character in raw Block: " + c, fName, currentLine, (charIndex - 1) - lineStartIndex);
                        }
                    }
                }
            }

            char c = src[charIndex];

            if(Character.isSpaceChar(c) || c == '\n' || c == '\r') {
                charIndex++;
                if(c == '\n') {
                    currentLine++;
                    lineStartIndex = charIndex;
                }
                continue;
            }
            if(Character.isLetter(c)) {
                while(Character.isDigit(src[charIndex]) || Character.isLetter(src[charIndex])) {
                    buf.append(src[charIndex++]);
                }
                if (keywords.containsKey(buf.toString())) {
                    tokens.add(keywords.get(buf.toString()).apply(currentLine, charIndex - lineStartIndex - buf.length()));
                } else {
                    tokens.add(new IdentifierToken(buf.toString(), currentLine, charIndex - lineStartIndex - buf.length()));
                }
                clear(buf);
            } else if(charIndex+1 < src.length && specialTokens.containsKey(String.valueOf(c) + src[charIndex + 1])) {
                tokens.add(specialTokens.get(String.valueOf(c) + src[charIndex + 1]).apply(currentLine, charIndex - lineStartIndex));
                charIndex += 2;
            } else if(specialTokens.containsKey(String.valueOf(c))) {
                tokens.add(specialTokens.get(String.valueOf(c)).apply(currentLine, charIndex - lineStartIndex));
                charIndex++;
            } else if(c == '"') {
                while((c = src[charIndex]) != '"') {
                    if(c == '\\') {
                        buf.append(src[charIndex++]);
                    }
                }
                tokens.add(Literals.STR.makeToken(buf.toString(), currentLine, charIndex - lineStartIndex - buf.length()));
                clear(buf);
            } else if(c == '\'') {
                charIndex++;
                tokens.add(Literals.CHAR.makeToken(src[charIndex++], currentLine, charIndex - lineStartIndex));
                if(src[charIndex++] != '\'') {
                    throw new TokenizationError("Expected closing single quote", fName, currentLine, charIndex - lineStartIndex);
                }
            } else if(Character.isDigit(c)) {
                boolean f = false;
                while(Character.isDigit(src[charIndex]) || src[charIndex] == '.') {
                    buf.append(src[charIndex++]);
                    if(src[charIndex] == '.'){
                        if(f) throw new TokenizationError("Extra Decimal in float", fName, currentLine, charIndex - lineStartIndex);
                        f = true;
                    }
                }
                if(f) {
                    tokens.add(Literals.FLOAT.makeToken(Float.parseFloat(buf.toString()), currentLine, charIndex - lineStartIndex - buf.length()));
                } else {
                    tokens.add(Literals.INT.makeToken(Integer.parseInt(buf.toString()), currentLine, charIndex - lineStartIndex - buf.length()));
                }
                clear(buf);
            } else {
                throw new TokenizationError("Unknown Error... buf: %s, c = %c, i = %d".formatted(buf.toString(), c, charIndex), fName, currentLine, charIndex - lineStartIndex);
            }
        }
        
        return Pair.of(fName, tokens);
    }
    
    private static void clear(StringBuffer buf) {
        buf.delete(0, buf.length());
        buf.setLength(0);
    }
    
    private static boolean shouldParseRaw(List<Token> tokens) {
        return tokens.size() >= 2 &&
                tokens.get(tokens.size() - 2) instanceof Keywords.KeywordToken m2 &&
                m2.getWord() == Keywords.RAW &&
                tokens.get(tokens.size() - 1) instanceof Special.SpecialToken m1 &&
                m1.getWord() == Special.OPEN_BRACE;
    }
}
