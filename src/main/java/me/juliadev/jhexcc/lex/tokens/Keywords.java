package me.juliadev.jhexcc.lex.tokens;

import lombok.Getter;
import lombok.ToString;
import me.juliadev.jhexcc.lex.Lexical;
import me.juliadev.jhexcc.lex.Token;

import java.util.Objects;
import java.util.function.BiFunction;

public enum Keywords {
    RETURN("return"),
    IF("if"),
    ELSE("else"),
    FOREACH( "for", "foreach"),
    IN("in"),
    RAW("raw"),
    PATTERN((l,c) -> new TypeToken("PAT", l, c), "pattern"),
    VOID((l,c) -> new TypeToken("VOID", l, c),"void"),
    NUM((l,c) -> new TypeToken("NUM", l, c),"num", "float", "int"),
    VEC((l,c) -> new TypeToken("VEC", l, c),"vec", "vector"),
    ENT((l,c) -> new TypeToken("ENT", l, c),"ent", "entity"),
    LIST((l,c) -> new TypeToken("LIST", l, c),"list");
    
    private final BiFunction<Integer, Integer, Token> tokenFactory;
    Keywords(BiFunction<Integer, Integer, Token> sup, String... s) {
        tokenFactory = sup;
        for (String s1 : s) {
            Lexical.keywords.put(s1, sup);
        }
    }
    
    Keywords(String... s) {
        tokenFactory = null;
        for (String s1 : s) {
            Lexical.keywords.put(s1, this::makeToken);
        }
    }
    
    public Token makeToken(int line, int column) {
        return tokenFactory != null ? tokenFactory.apply(line, column) : new KeywordToken(this, line, column);
    }

    @ToString(callSuper = true)
    public static class KeywordToken extends Token {
        @Getter Keywords word;
        
        private KeywordToken(Keywords word, int line, int column) {
            super(line, column);
            this.word = word;
        }
        
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            KeywordToken that = (KeywordToken) o;
            return word == that.word;
        }
        
        @Override
        public int hashCode() {
            return Objects.hash(word);
        }

        @Override
        public String stringyVersion() {
            return word.name();
        }
    }
}
