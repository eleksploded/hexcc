package me.juliadev.jhexcc.lex.tokens;

import lombok.Getter;
import lombok.ToString;
import me.juliadev.jhexcc.lex.Lexical;
import me.juliadev.jhexcc.lex.Token;

import java.util.Objects;

public enum Special {
    SEMI(";"),
    COMMA(","),
    OPEN_PAREN("("),
    CLOSE_PAREN(")"),
    OPEN_BRACE("{"),
    CLOSE_BRACE("}"),
    ASSIGN("="),
    PLUS("+"),
    STAR("*"),
    SUB("-"),
    DIV("/"),
    NOT("!"),
    MOD("%"),
    AND("&&"),
    OR("||"),
    EQU("=="),
    SHIFT_LEFT("<<"),
    SHIFT_RIGHT(">>"),
    GT(">"),
    LT("<"),
    GTE(">="),
    LTE("<="),
    NOT_EQU("!="),
    
    DOLLAR("$"),
    POUND("#"),
    OPEN_SQUARE_BRACE("["),
    CLOSE_SQUARE_BRACE("]"),
    OPEN_TRI_BRACE("<"),
    CLOSE_TRI_BRACE(">");

    @Getter
    final String symbol;
    Special(String symbol) {
        this.symbol = symbol;
        Lexical.specialTokens.put(symbol, this::makeToken);
    }
    
    
    public SpecialToken makeToken(int line, int column) {
        return new SpecialToken(this, line, column);
    }
    
    @Getter @ToString(callSuper = true)
    public static class SpecialToken extends Token {
        Special word;
        
        private SpecialToken(Special word, int line, int column) {
            super(line, column);
            this.word = word;
        }
        
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            SpecialToken that = (SpecialToken) o;
            return word == that.word;
        }
        
        @Override
        public int hashCode() {
            return Objects.hash(word);
        }


        @Override
        public String stringyVersion() {
            return word.symbol;
        }
    }
}
