package me.juliadev.jhexcc.lex.tokens;

import lombok.ToString;
import me.juliadev.jhexcc.lex.Token;

import java.util.Objects;

public enum Literals {
    STR(String.class),
    CHAR(Character.class),
    INT(Integer.class),
    FLOAT(Float.class);

    private final Class<?> type;
    Literals(Class<?> type) {
        this.type = type;
    }
    
    public <T> LiteralToken<T> makeToken(T value, int line, int column) {
        if(!value.getClass().equals(this.type))
            throw new IllegalArgumentException("invalid type. wanted " + this.type.getName() + ", got " + value.getClass().getName());
        return new LiteralToken<>(value, line, column);
    }

    @ToString(callSuper = true)
    public class LiteralToken<T> extends Token {
        T value;
        
        LiteralToken(T value, int line, int column) {
            super(line, column);
            this.value = value;
        }
        
        public Class<T> getType() {
            return (Class<T>) type;
        }
        
        public T getValue() {
            return value;
        }

        public void updateValue(T value) {
            this.value = value;
        }
        
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            LiteralToken<?> that = (LiteralToken<?>) o;
            if (getType() != that.getType()) return false;
            return Objects.equals(value, that.value);
        }
        
        @Override
        public int hashCode() {
            return Objects.hash(getType(), value);
        }

        @Override
        public String stringyVersion() {
            return "Literal: " + String.valueOf(value);
        }
    }
}
