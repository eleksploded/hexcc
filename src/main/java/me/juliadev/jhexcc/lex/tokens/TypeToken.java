package me.juliadev.jhexcc.lex.tokens;

import lombok.ToString;
import me.juliadev.jhexcc.lex.Token;

import java.util.Objects;

@ToString(callSuper = true)
public class TypeToken extends Token {

    public Types type() {
        return type;
    }

    @Override
    public String stringyVersion() {
        return type.name();
    }

    public enum Types {
        VOID, NUM, VEC, ENT, PAT, LIST, BOOL;
    }
    
    Types type;
    public TypeToken(String type, int line, int column) {
        super(line, column);
        this.type = Types.valueOf(type);
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeToken typeToken = (TypeToken) o;
        return type == typeToken.type;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(type);
    }
}
