package me.juliadev.jhexcc.lex.tokens;

import lombok.Getter;
import lombok.ToString;
import me.juliadev.jhexcc.lex.Token;

import java.util.Objects;

@Getter @ToString(callSuper = true)
public class RawEntryToken extends Token {
    String entry;

    public RawEntryToken(String entry, int line, int column) {
        super(line, column);
        this.entry = entry;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RawEntryToken that = (RawEntryToken) o;
        return Objects.equals(entry, that.entry);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(entry);
    }

    @Override
    public String stringyVersion() {
        return "Raw Block";
    }
}
