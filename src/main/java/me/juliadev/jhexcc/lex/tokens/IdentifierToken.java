package me.juliadev.jhexcc.lex.tokens;

import lombok.Getter;
import lombok.ToString;
import me.juliadev.jhexcc.lex.Token;

import java.util.Objects;

@Getter @ToString(callSuper = true)
public class IdentifierToken extends Token {
    String identifier;
    
    public IdentifierToken(String identifier, int line, int column) {
        super(line, column);
        this.identifier = identifier;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdentifierToken that = (IdentifierToken) o;
        return Objects.equals(identifier, that.identifier);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(identifier);
    }

    @Override
    public String stringyVersion() {
        return "Identifier: " + identifier;
    }
}
