package me.juliadev.jhexcc.lex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor @Getter @ToString
public abstract class Token {
    int line, column;

    public abstract String stringyVersion();
}
