# Optimizations
These are all the built-in optimizations in JHexCC.

Passes are taken as long as the result IrHexC changed during the last pass, or until an optional maximum number of passes are taken.
For information on how optimizations are implemented, see [Making Optimizations](makingOptimizations.md).

*Not all optimizations are implemented yet, due to the switch to json driven optimizations.*

### VectorReflectionsOptimization
A collection of optimizations that converts Vector literals of an on-axis literal into the corresponding Vector Reflection pattern.
- [VectorReflectionMX](../../../../../resources/optimizations/VectorReflectionMX.json)
- [VectorReflectionX](../../../../../resources/optimizations/VectorReflectionX.json)
- [VectorReflectionMY](../../../../../resources/optimizations/VectorReflectionMY.json)
- [VectorReflectionY](../../../../../resources/optimizations/VectorReflectionY.json)
- [VectorReflectionMZ](../../../../../resources/optimizations/VectorReflectionMZ.json)
- [VectorReflectionZ](../../../../../resources/optimizations/VectorReflectionZ.json)

### WriteRead
Converts a consecutive write and then read of a single identifier to a [DUP](../codegen/ir_hexc/SimpleActions.java) and Write to remove the need to read the identifier.
The DUP eventually gets replaced with *Gemini Decomposition* in the final hex.

### RemoveUnusedWrites
Removes Writes for identifiers that are never read.

### RemoveUnusedIdentifiers
Removes the creation of identifiers that are only ever used on the stack, and never read or written to/from the ravenmind memory.

### MultiDrop
Converts multiple consecutive drop actions into a singular MultiDrop action. 
The Hex Generator eventually converts these into *Bookkeeper's Gambit* with the correct number of `v`s

### SinglePatternFunction
Appends a *Flock's Disintegration* after a function definition that consists of a single pattern. 
This allows Hermes to execute it without consuming a meta-evaluation, as it is a pattern instead of a list.