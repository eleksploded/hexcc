# Making Optimizations

Optimizations are stored in a json format, which either contain a search array and replacement array, 
or a reference to class extending [Optimization](Optimization.java).

## Code Optimization

To make a code optimization, create a class extending [Optimization.java](Optimization.java) and 
override the `process(Scope, List<IrHexCAction>)` method. This method is provided with access to the root scope,
and a mutable list of actions to modify as the optimization sees fit.

To provide this class to JHexCC, create a json file in the optimizations folder which just has the field
`code`, and the value of the fully qualified class name of the optimization.
### Example 
```json
{
  "code": "package.path.to.MyOptimization"
}
```

## Search Replacement Optimization

Search Replacement Optimization provide an array of actions to search for in the IrHexC, and a replacement
when that array is found. 

### Example
```json
{
  "search": [
    {
      "action": "literal",
      "value": 1
    },
    {
      "action": "literal",
      "value": 0
    },
    {
      "action": "literal",
      "value": 0
    },
    {
      "action": "simple",
      "type": "make_vec"
    }
  ],
  "replace": [
    {
      "action": "simple",
      "type": "vec_refl_x"
    }
  ]
}
```

### Search
The `search` field contains an array of actions to find. An action is required to have the field `action`.
This field specifies what type of action to be looking for, and is the class name of the action without the leading IrHexC, or
a `*` which specifies any action can match.

After the `action` field, any fields in the action can be specified to further narrow down what is searched for, eg specific literals.
These fields can also have the value of `*` which just searches for the presence of the field, and doesn't care what its value is.

### Replacement
The `replace` field is an array of actions to replace the found actions in the `search` field. These actions are
structured the same way, but wildcards are not supported. However, fields can be set to the same value of searched actions
using a special string starting with a `$`. This string is formatted as `$index.field` where index is the index of the action in
the `search` field, and `field` is the field name of said action. Ex: `$0.value` would be the value field in the action found at index 0.

### Simple Actions
[Simple actions](../codegen/ir_hexc/SimpleActions.java) are handled differently than others due to them being an enum instead of a class. They are specified for search or replacement
with the `action` value being "simple", and a `type` field corresponding to the enum name.