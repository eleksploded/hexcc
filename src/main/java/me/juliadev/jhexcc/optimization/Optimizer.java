package me.juliadev.jhexcc.optimization;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import me.juliadev.jhexcc.codegen.ir_hexc.IrHexCAction;
import me.juliadev.jhexcc.codegen.ir_hexc.IrHexCProgram;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Optimizer {
    private static final Moshi moshi = new Moshi.Builder().build();
    
    private static List<Optimization> allOptimizations;
    
    public static List<Optimization> getAllOptimizations(String extraFolder) {
        if(allOptimizations == null) {
            JsonAdapter<Optimization> adapter = moshi.adapter(Optimization.class);
            allOptimizations = new ArrayList<>();
            
            //built in opts
            for (String optFile : getResourceFiles("optimizations")) {
                try(InputStream is = getResourceAsStream("optimizations/" + optFile)) {
                    String result = new BufferedReader(new InputStreamReader(is)).lines().collect(Collectors.joining("\n"));
                    Optimization e = adapter.fromJson(result);
                    e.name = optFile.replace(".json", "");
                    allOptimizations.add(e);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            
            //extras
            if(extraFolder != null && !extraFolder.isEmpty()) {
                File extra = new File("./" + extraFolder);
                for (File file : extra.listFiles()) {
                    if(file.isDirectory()) continue; //maybe support directories later?
                    
                    if(file.getName().endsWith(".json")) {
                        try {
                            String s = Files.readString(file.toPath());
                            Optimization e = adapter.fromJson(s);
                            e.name = file.getName().replace(".json", "");
                            allOptimizations.add(e);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                }
            }
        }
        return allOptimizations;
    }
    
    public static void runOptimizations(List<String> toRun, String extraFolder, IrHexCProgram pgrm, int maxPasses) {
        if(maxPasses == 0) return;
        List<Optimization> optsToRun = getAllOptimizations(extraFolder).stream().filter(o -> toRun.contains(o.name)).toList();
        
        int current = -1;
        do {
            current = pgrm.hashCode();
            List<IrHexCAction> working = new ArrayList<>(pgrm.getActions());
            for (Optimization opt : optsToRun) {
                opt.process(pgrm.getRootScope(), working);
            }
            
            pgrm.setActions(working);
            pgrm.setOptimizationPasses(pgrm.getOptimizationPasses() + 1);

            if(maxPasses > 0 && pgrm.getOptimizationPasses() >= maxPasses) break;
        } while(current != pgrm.hashCode());
    }
    
    private static List<String> getResourceFiles(String path) {
        List<String> filenames = new ArrayList<>();
        
        try (
                InputStream in = getResourceAsStream(path);
                BufferedReader br = new BufferedReader(new InputStreamReader(in))
        ) {
            String resource;
            
            while ((resource = br.readLine()) != null) {
                filenames.add(resource);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return filenames;
    }
    
    private static InputStream getResourceAsStream(String resource) {
        final InputStream in = Optimizer.class.getClassLoader().getResourceAsStream(resource);
        return in == null ? Optimizer.class.getResourceAsStream(resource) : in;
    }
}
