package me.juliadev.jhexcc.optimization;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import me.juliadev.jhexcc.codegen.ir_hexc.IrHexCAction;
import me.juliadev.jhexcc.codegen.ir_hexc.Scope;
import me.juliadev.jhexcc.codegen.ir_hexc.SimpleActions;
import me.juliadev.jhexcc.lex.tokens.TypeToken;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@NoArgsConstructor
public class Optimization {
    private Map<String, Object>[] search;
    private Map<String, Object>[] replace;
    private String code;
    
    private transient List<IrHexCAction> foundActions = new ArrayList<>();
    @Getter
    transient String name;
    
    private boolean verifyActions() {
        for (Map<String, Object> action : search) {
            if (!action.containsKey("action")) {
                return false;
            }
        }
        for (Map<String, Object> action : replace) {
            if (!action.containsKey("action")) {
                return false;
            }
        }
        return true;
    }
    
    public void process(Scope rootScope, List<IrHexCAction> actions) {
        if(code != null && !code.isBlank()) {
            try {
                Class<?> codeClass = Class.forName(code);
                Object o = codeClass.getConstructor().newInstance();
                if(o instanceof Optimization opt) {
                    opt.process(rootScope, actions);
                } else {
                    throw new RuntimeException("Code Optimizations should extend Optimization");
                }
            } catch (ReflectiveOperationException e) {
                throw new RuntimeException(e);
            }
            return;
        }
        
        if(!verifyActions()) {
            throw new RuntimeException("Search-Replace Optimization verification failed in " + name);
        }
        
        for (int i = 0; i < actions.size(); i++) {
            if(search(i, actions)) {
                replace(i, actions);
            }
        }
    }
    
    @SneakyThrows
    private void replace(int index, List<IrHexCAction> actions) {
        for (int i = 0; i < search.length; i++) {
            actions.remove(index);
        }
        
        for (int i = replace.length - 1; i >= 0; i--) {
            actions.add(index, makeAction(replace[i]));
        }
    }
    
    private IrHexCAction makeAction(Map<String, Object> actionData) throws ReflectiveOperationException {
        if(actionData.get("action").toString().equalsIgnoreCase("simple")) {
            try {
                return SimpleActions.valueOf(actionData.getOrDefault("type", "").toString().toUpperCase());
            } catch (IllegalArgumentException e) {
                throw new RuntimeException("Unknown simple action type: " + actionData.getOrDefault("type", "null"));
            }
        } else {
            Class<?> actionClass = findActionClass(actionData.get("action").toString());
            Object action = actionClass.getConstructor().newInstance();
            if(!(action instanceof IrHexCAction)) {
                throw new RuntimeException("Unknown action type: " + actionData.get("action"));
            }
            
            for (Map.Entry<String, Object> data : actionData.entrySet()) {
                try {
                    Field field = actionClass.getDeclaredField(data.getKey());
                    field.setAccessible(true);
                    if(data.getValue().toString().startsWith("$")) {
                        String[] split = data.getValue().toString().substring(1).split("\\.");
                        int index = Integer.parseInt(split[0]);
                        Field declaredField = foundActions.get(index).getClass().getDeclaredField(split[1]);
                        declaredField.setAccessible(true);
                        field.set(action, declaredField.get(foundActions.get(index)));
                    } else {
                        field.set(action, data.getValue());
                    }
                } catch (NoSuchFieldException e) {
                    throw new RuntimeException("Unknown field for action " + actionClass.getName() + ": " + data.getKey());
                } catch (IllegalAccessException e) {
                    throw new ReflectiveOperationException(e);
                }
            }
            return (IrHexCAction) action;
        }
    }
    
    private boolean search(int index, List<IrHexCAction> action) {
        for (int i = 0; i < search.length; i++) {
            if(!matches(search[i], action.get(index + i))) {
                foundActions.clear();
                return false;
            }
            foundActions.add(action.get(index + i));
        }
        return true;
    }
    
    @SneakyThrows
    private boolean matches(Map<String, Object> search, IrHexCAction action) {
        if (action instanceof SimpleActions a) return matchesSimpleAction(search, a);
        if (search.get("action").toString().equalsIgnoreCase("*")) return true;
        
        Class<? extends IrHexCAction> actionClass = action.getClass();
        String actionName = actionClass.getSimpleName().replace("IrHexC", "").toLowerCase();
        if(!search.get("action").toString().equalsIgnoreCase(actionName)) {
            return false;
        }
        
        for(String fieldName : search.keySet()) {
            if(fieldName.equalsIgnoreCase("action")) continue;
            
            try {
                Field field = actionClass.getDeclaredField(fieldName);
                field.setAccessible(true);
                
                if (!search.get(fieldName).equals("*") && !equal(stringify(field.get(action)), search.get(fieldName))) {
                    return false;
                }
            } catch (NoSuchFieldException e) {
                return false;
            }
        }
        
        return true;
    }
    
    private boolean matchesSimpleAction(Map<String, Object> search, SimpleActions a) {
        if(!search.get("action").toString().equalsIgnoreCase("simple")) return false;
        
        if(search.containsKey("type") && !search.get("type").toString().toUpperCase().equals(a.name())) {
            return false;
        }
        
        return true;
    }
    
    @SneakyThrows
    private Object getField(int index, String fieldName) {
        try {
            Field declaredField = foundActions.get(index).getClass().getDeclaredField(fieldName);
            declaredField.setAccessible(true);
            return declaredField.get(foundActions.get(index));
        } catch (NoSuchFieldException e) {
            return null;
        }
    }
    
    private Class<?> findActionClass(String actionName) {
        String basePkg = "me.juliadev.jhexcc.codegen.ir_hexc.";
        List<String> searchList = List.of(actionName, "IrHexC" + actionName, "scope_actions." + actionName, "scope_actions.IrHexC" + actionName);
        
        for (String classSearch : searchList) {
            try {
                return Class.forName(basePkg + classSearch);
            } catch (ClassNotFoundException ignored) {}
        }
        
        throw new RuntimeException("Unknown action name: " + actionName);
    }

    private boolean equal(Object o1, Object o2) {
        if(o1 instanceof Integer i && o2 instanceof Double d) {
            return i.intValue() == d.doubleValue();
        }
        if(o1 instanceof Double i && o2 instanceof Double d) {
            return Math.abs(i - d) < Math.max(Math.ulp(i), Math.ulp(d));
        }

        return Objects.equals(o1, o2);
    }
    
    private Object stringify(Object o) {
        if(o instanceof TypeToken.Types type) return type.name().toLowerCase();
        
        return o;
    }
}
