package me.juliadev.jhexcc.util;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

@Getter
@EqualsAndHashCode
@ToString
public class Pair<L, R> implements Map.Entry<L,R> {
    
    L left;
    R right;
    
    public Pair(L left, R right) {
        this.left = left;
        this.right = right;
    }

    @SafeVarargs
    public static <K, V> Map<K, V> makeMap(Pair<K, V>... pair) {
        Map<K, V> map = new HashMap<>();
        for(Pair<K, V> p : pair) {
            map.put(p.getKey(), p.getValue());
        }
        return map;
    }
    
    @Override
    public L getKey() {
        return left;
    }
    
    @Override
    public R getValue() {
        return right;
    }
    
    @Override
    public R setValue(R value) {
        R oldValue = right;
        right = (R) value;
        return oldValue;
    }
    
    public static <L1, R1> Pair<L1, R1> of(L1 l, R1 r) {
        return new Pair<>(l,r);
    }




}
