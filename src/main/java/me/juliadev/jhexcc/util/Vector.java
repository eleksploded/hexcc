package me.juliadev.jhexcc.util;

import java.util.Objects;


public record Vector(int x, int y, int z) {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vector vector)) return false;
        return x == vector.x && y == vector.y && z == vector.z;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }
}
