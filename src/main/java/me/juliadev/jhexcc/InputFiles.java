package me.juliadev.jhexcc;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InputFiles {
    private static final Map<String, List<String>> fileData = new HashMap<>();

    public static String readString(File file) throws IOException {
        if(!fileData.containsKey(file.getPath())) {
            fileData.put(file.getPath(), Collections.unmodifiableList(Files.readAllLines(file.toPath())));
        }
        return String.join("\n", fileData.get(file.getPath()));
    }

    public static String readString(String path) {
        return fileData.containsKey(path) ? String.join("\n", fileData.get(path)) : null;
    }

    public static List<String> readAllLines(File file) throws IOException {
        if(!fileData.containsKey(file.getPath())) {
            fileData.put(file.getPath(), Collections.unmodifiableList(Files.readAllLines(file.toPath())));
        }
        return fileData.get(file.getPath());
    }

    public static List<String> readAllLines(String path) {
        return fileData.getOrDefault(path, null);
    }
}
