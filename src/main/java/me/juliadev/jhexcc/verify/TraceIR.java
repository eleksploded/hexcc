package me.juliadev.jhexcc.verify;

public class TraceIR {/*
    private static final int MAX_DEPTH = 2;
    private static final IdentifierMetadata mainMeta = new FunctionMetadata(
            "main", List.of(TypeToken.Types.VOID), 0, 0, new ArrayList<>()
    );
    
    public static void trace(List<IrHexCAction> irhexc) {
        Stack<Integer> returnAddress = new Stack<>();
        returnAddress.push(irhexc.size());
        Stack<IdentifierMetadata> stack = new Stack<>();
        
        int exec = findFuncMeta(mainMeta, irhexc) + 1;
        
        while(exec < irhexc.size()) {
            IrHexCAction action = irhexc.get(exec);
            
            if(returnAddress.size() > MAX_DEPTH) throw new RuntimeException("Max depth exceeded");
            
            if(action instanceof IdentifierMap.ReadIdentifier read) {
                stack.push(read.meta());
            } else if(action instanceof SimpleActions sp && sp == SimpleActions.EXEC) {
                IdentifierMetadata id = stack.pop();
                if(!(id instanceof FunctionMetadata)) throw new RuntimeException("Tried to exec a VAR");
                returnAddress.push(exec);
                exec = findFuncMeta(id, irhexc);
                
            } else if(action instanceof IdentifierMap.CloseScope) {
                exec = returnAddress.pop();
            } else if(action instanceof IdentifierMap.CreateNewScope) {
                while(!(action instanceof IdentifierMap.CloseScope)) {
                    exec++;
                    action = irhexc.get(exec);
                }
            } else if(action instanceof IdentifierMap.CreateFuncScope) {
                throw new RuntimeException("Can't create function in a function");
            } else if(action instanceof IdentifierMap.WriteIdentifier) {
                stack.pop();
            } else if(action instanceof SimpleActions sp && sp == SimpleActions.DUP) {
                stack.push(stack.peek());
            }
            
            exec++;
        }
    }
    
    private static int findFuncMeta(IdentifierMetadata meta, List<IrHexCAction> irhexc) {
        for (int i = irhexc.size() - 1; i >= 0; i--) {
            IrHexCAction act = irhexc.get(i);
            if(act instanceof IdentifierMap.CreateFuncScope cfs) {
                if(matchIgnoreAddr(cfs.getMeta(), meta)) {
                    return i;
                }
            }
        }
        throw new RuntimeException("Cant find function " + meta.getName());
    }
    
    private static boolean matchIgnoreAddr(IdentifierMetadata meta1, IdentifierMetadata meta2) {
        return meta1.getName().equals(meta2.getName()) &&
                meta1.getType() == meta2.getType() &&
                meta1.getValueTypes().equals(meta2.getValueTypes());
    }*/
}
