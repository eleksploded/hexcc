package me.juliadev.jhexcc.parse.nodes;

import lombok.Data;
import lombok.EqualsAndHashCode;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.Keywords;
import me.juliadev.jhexcc.lex.tokens.Special;
import me.juliadev.jhexcc.parse.ExprParser;
import me.juliadev.jhexcc.parse.ParseError;
import me.juliadev.jhexcc.parse.ParseUtil;

import java.util.LinkedList;

@Data
@EqualsAndHashCode(callSuper = false)
public class NodeConditional extends NodeStmtList.NodeStmtParam {
    NodeExpr condition;
    NodeStmtList ifTrue;
    NodeStmtList ifFalse;

    NodeConditional(LinkedList<Token> tokens) {
        if(ParseUtil.isKeywordToken(tokens, Keywords.IF)) tokens.pop();
        else throw new ParseError("Unable to find if keyword for conditional", tokens);

        ParseUtil.pop(tokens, Special.OPEN_PAREN);

        condition = ExprParser.parse(tokens);
        
        ParseUtil.pop(tokens, Special.CLOSE_PAREN);

        ifTrue = new NodeStmtList(tokens);

        if(ParseUtil.isKeywordToken(tokens, Keywords.ELSE)) {
            tokens.pop();
            ifFalse = new NodeStmtList(tokens);
        }
    }
}
