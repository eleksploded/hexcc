package me.juliadev.jhexcc.parse.nodes;

import lombok.Data;
import lombok.EqualsAndHashCode;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.Keywords;
import me.juliadev.jhexcc.lex.tokens.Special;
import me.juliadev.jhexcc.parse.ExprParser;
import me.juliadev.jhexcc.parse.ParseUtil;

import java.util.LinkedList;

@Data
@EqualsAndHashCode(callSuper = false)
public class NodeReturn extends NodeStmtList.NodeStmtParam {
    NodeExpr expr;

    NodeReturn(LinkedList<Token> tokens) {
        ParseUtil.pop(tokens, Keywords.RETURN);
        expr = (ExprParser.parse(tokens));
        ParseUtil.pop(tokens, Special.SEMI);
    }
}
