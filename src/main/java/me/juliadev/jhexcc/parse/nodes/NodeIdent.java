package me.juliadev.jhexcc.parse.nodes;

import lombok.Data;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.IdentifierToken;
import me.juliadev.jhexcc.parse.ParseError;

import java.util.LinkedList;

@Data
public class NodeIdent implements NodeExpr {
    String identifier;

    public NodeIdent(LinkedList<Token> tokens) {
        if(tokens.peek() instanceof IdentifierToken t) {
            identifier = t.getIdentifier();
            tokens.pop();
        } else {
            throw new ParseError("Unable to find identifier token", tokens);
        }
    }
}
