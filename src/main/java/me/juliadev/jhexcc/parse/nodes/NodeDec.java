package me.juliadev.jhexcc.parse.nodes;

import lombok.Data;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.Special;
import me.juliadev.jhexcc.parse.ParseError;
import me.juliadev.jhexcc.parse.ParseUtil;

import java.util.LinkedList;
import java.util.Objects;

@Data
public class NodeDec {
    private static final Token FuncSplit = Special.OPEN_PAREN.makeToken(0,0);
    private static final Token VarSplit1 = Special.ASSIGN.makeToken(0,0);
    private static final Token VarSplit2 = Special.SEMI.makeToken(0,0);
    
    NodeDecParam param;
    
    public NodeDec(LinkedList<Token> tokens) {
        Token split = ParseUtil.peekAheadUntil(tokens, FuncSplit, VarSplit1, VarSplit2);
        
        if(Objects.equals(split, FuncSplit)) {
            param = new NodeFuncDec(tokens);
        } else if(Objects.equals(split, VarSplit1) || Objects.equals(split, VarSplit2)) {
            param = new NodeVarDec(tokens);
        } else {
            throw new ParseError("Unable to determine declaration type", tokens);
        }
    }
    
    public interface NodeDecParam {}
}


