package me.juliadev.jhexcc.parse.nodes;

import lombok.Data;
import lombok.EqualsAndHashCode;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.Special;
import me.juliadev.jhexcc.parse.ExprParser;
import me.juliadev.jhexcc.parse.ParseUtil;

import java.util.LinkedList;

@Data
@EqualsAndHashCode(callSuper = false)
public class NodeVarDec extends NodeStmtList.NodeStmtParam implements NodeDec.NodeDecParam {
    NodeType type;
    NodeIdent ident;
    NodeExpr expr;
    
    public NodeVarDec(LinkedList<Token> tokens) {
        type = new NodeType(tokens);
        ident = new NodeIdent(tokens);

        if(ParseUtil.isSpecialToken(tokens, Special.ASSIGN)) {
            tokens.pop();
            expr = ExprParser.parse(tokens);
        }

        ParseUtil.pop(tokens, Special.SEMI);
    }
}
