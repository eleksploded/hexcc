package me.juliadev.jhexcc.parse.nodes;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.Special;
import me.juliadev.jhexcc.parse.ExprParser;
import me.juliadev.jhexcc.parse.ParseUtil;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@EqualsAndHashCode
@Getter
public class NodeList implements NodeExpr {
    List<NodeExpr> values = new ArrayList<>();

    public NodeList(LinkedList<Token> tokens) {
        ParseUtil.pop(tokens, Special.OPEN_SQUARE_BRACE);
        while(!ParseUtil.isSpecialToken(tokens, Special.CLOSE_SQUARE_BRACE)) {
            values.add(ExprParser.parse(tokens));
            if(!ParseUtil.isSpecialToken(tokens, Special.CLOSE_SQUARE_BRACE)) {
                ParseUtil.pop(tokens, Special.COMMA);
            }
        }
        ParseUtil.pop(tokens, Special.CLOSE_SQUARE_BRACE);
    }
}
