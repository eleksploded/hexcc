package me.juliadev.jhexcc.parse.nodes;

import lombok.Data;
import lombok.EqualsAndHashCode;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.Keywords;
import me.juliadev.jhexcc.lex.tokens.RawEntryToken;
import me.juliadev.jhexcc.lex.tokens.Special;
import me.juliadev.jhexcc.parse.ParseUtil;
import me.juliadev.jhexcc.util.Pair;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class NodeRawStmt extends NodeStmtList.NodeStmtParam {
    List<Pair<Instruction, String>> instructions;
    
    NodeRawStmt(LinkedList<Token> tokens) {
        ParseUtil.pop(tokens, Keywords.RAW);
        ParseUtil.pop(tokens, Special.OPEN_BRACE);
        
        instructions = new ArrayList<>();
        while(tokens.peek() instanceof RawEntryToken entry) {
            tokens.pop();
            String inst = entry.getEntry();
            
            if(inst.startsWith("#(") && inst.endsWith(")")) {
                instructions.add(Pair.of(Instruction.WRITE, inst.substring(2, inst.length() - 1)));
            } else if(inst.startsWith("$(") && inst.endsWith(")")) {
                instructions.add(Pair.of(Instruction.READ, inst.substring(2, inst.length() - 1)));
            } else if(inst.startsWith("/(") && inst.endsWith(")")) {
                instructions.add(Pair.of(Instruction.EMBED, inst.substring(2, inst.length() - 1)));
            } else {
                instructions.add(Pair.of(Instruction.HEX, inst));
            }
        }
        
        ParseUtil.pop(tokens, Special.CLOSE_BRACE);
    }
    
    public enum Instruction {
        HEX, READ, EMBED, WRITE
    }
}
