package me.juliadev.jhexcc.parse.nodes;

import lombok.Data;
import me.juliadev.jhexcc.lex.Token;

import java.util.LinkedList;

@Data
public class NodeParam {
    NodeType type;
    NodeIdent ident;

    public NodeParam(LinkedList<Token> tokens) {
        type = new NodeType(tokens);
        ident = new NodeIdent(tokens);
    }
}
