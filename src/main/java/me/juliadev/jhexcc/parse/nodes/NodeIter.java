package me.juliadev.jhexcc.parse.nodes;

import lombok.Data;
import lombok.EqualsAndHashCode;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.Keywords;
import me.juliadev.jhexcc.lex.tokens.Special;
import me.juliadev.jhexcc.parse.ExprParser;
import me.juliadev.jhexcc.parse.ParseUtil;

import java.util.LinkedList;

@Data
@EqualsAndHashCode(callSuper = false)
public class NodeIter extends NodeStmtList.NodeStmtParam {
    NodeType type;
    NodeIdent ident;
    NodeExpr expr;
    NodeStmtList stmts;

    NodeIter(LinkedList<Token> tokens) {
        ParseUtil.pop(tokens, Keywords.FOREACH);
        ParseUtil.pop(tokens, Special.OPEN_PAREN);
        type = new NodeType(tokens);
        ident = new NodeIdent(tokens);
        ParseUtil.pop(tokens, Keywords.IN);
        expr = ExprParser.parse(tokens);
        ParseUtil.pop(tokens, Special.CLOSE_PAREN);
        stmts = new NodeStmtList(tokens);
    }
}
