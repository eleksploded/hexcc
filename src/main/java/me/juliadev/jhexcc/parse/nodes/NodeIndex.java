package me.juliadev.jhexcc.parse.nodes;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.Special;
import me.juliadev.jhexcc.lex.tokens.TypeToken;
import me.juliadev.jhexcc.parse.ExprParser;
import me.juliadev.jhexcc.parse.ParseError;
import me.juliadev.jhexcc.parse.ParseUtil;

import java.util.LinkedList;

@EqualsAndHashCode @Getter
public class NodeIndex implements NodeExpr {
    NodeIdent ident;
    NodeExpr index;
    TypeToken.Types type;
    
    public NodeIndex(LinkedList<Token> tokens) {
        ident = new NodeIdent(tokens);
        ParseUtil.pop(tokens, Special.OPEN_TRI_BRACE);
        if(tokens.peek() instanceof TypeToken k) {
            type = k.type();
            tokens.pop();
        } else {
            throw new ParseError("Invalid type: " + type, tokens);
        }
        ParseUtil.pop(tokens, Special.CLOSE_TRI_BRACE);
        ParseUtil.pop(tokens, Special.OPEN_SQUARE_BRACE);
        index = ExprParser.parse(tokens);
        ParseUtil.pop(tokens, Special.CLOSE_SQUARE_BRACE);
    }
}
