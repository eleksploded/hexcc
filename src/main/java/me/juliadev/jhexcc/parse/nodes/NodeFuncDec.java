package me.juliadev.jhexcc.parse.nodes;

import lombok.Data;
import lombok.EqualsAndHashCode;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.Special;
import me.juliadev.jhexcc.parse.ParseUtil;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@EqualsAndHashCode(callSuper = false)
@Data
public class NodeFuncDec implements NodeDec.NodeDecParam {
    NodeType type;
    NodeIdent ident;
    List<NodeParam> params;
    NodeStmtList stmts;
    
    public NodeFuncDec(LinkedList<Token> tokens) {
        type = new NodeType(tokens);
        ident = new NodeIdent(tokens);
        
        ParseUtil.pop(tokens, Special.OPEN_PAREN);

        params = new ArrayList<>();
        while(!ParseUtil.isSpecialToken(tokens, Special.CLOSE_PAREN)) {
            params.add(new NodeParam(tokens));
            if(ParseUtil.isSpecialToken(tokens, Special.COMMA)) tokens.pop();
        }
        tokens.pop();

        stmts = new NodeStmtList(tokens);
    }
}
