package me.juliadev.jhexcc.parse.nodes;

import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.Special;
import me.juliadev.jhexcc.parse.ExprParser;
import me.juliadev.jhexcc.parse.ParseError;

import java.util.LinkedList;

public class NodeOper {
    Special op;

    public NodeOper(LinkedList<Token> tokens) {
        if(ExprParser.isBiOp(tokens.peek())) {
            op = ((Special.SpecialToken) tokens.pop()).getWord();
        } else {
            throw new ParseError("Invalid Operator", tokens);
        }
    }

    public Special operator() {
        return op;
    }
}
