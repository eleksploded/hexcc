package me.juliadev.jhexcc.parse.nodes;

import lombok.Data;
import lombok.EqualsAndHashCode;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.Special;
import me.juliadev.jhexcc.parse.ExprParser;
import me.juliadev.jhexcc.parse.ParseUtil;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class NodeFuncCall extends NodeStmtList.NodeStmtParam implements NodeExpr {
    NodeIdent ident;
    List<NodeExpr> params;

    public NodeFuncCall(LinkedList<Token> tokens) {
        ident = new NodeIdent(tokens);
        
        ParseUtil.pop(tokens, Special.OPEN_PAREN);

        params = new ArrayList<>();
        while(!ParseUtil.isSpecialToken(tokens, Special.CLOSE_PAREN)) {
            params.add(ExprParser.parse(tokens));

            if(ParseUtil.isSpecialToken(tokens, Special.COMMA)) tokens.pop();
        }
        tokens.pop();
    }
}
