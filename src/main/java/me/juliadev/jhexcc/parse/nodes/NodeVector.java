package me.juliadev.jhexcc.parse.nodes;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.Special;
import me.juliadev.jhexcc.parse.ExprParser;
import me.juliadev.jhexcc.parse.ParseUtil;

import java.util.LinkedList;

@Getter
@EqualsAndHashCode(callSuper = false)
public class NodeVector implements NodeExpr {
    final NodeExpr x, y, z;

    public NodeVector(LinkedList<Token> tokens) {
        ParseUtil.pop(tokens, Special.OPEN_BRACE);
        x = ExprParser.parse(tokens);
        ParseUtil.pop(tokens, Special.COMMA);
        y = ExprParser.parse(tokens);
        ParseUtil.pop(tokens, Special.COMMA);
        z = ExprParser.parse(tokens);
        ParseUtil.pop(tokens, Special.CLOSE_BRACE);
    }
}
