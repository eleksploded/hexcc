package me.juliadev.jhexcc.parse.nodes;

import lombok.EqualsAndHashCode;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.Literals;
import me.juliadev.jhexcc.lex.tokens.Special;
import me.juliadev.jhexcc.parse.ParseError;
import me.juliadev.jhexcc.parse.ParseUtil;

import java.util.LinkedList;

@EqualsAndHashCode(callSuper = false)
public class NodeLiteral implements NodeExpr {
    Literals.LiteralToken<?> literal;

    public NodeLiteral(LinkedList<Token> tokens) {
        boolean negative = false;
        if(ParseUtil.isSpecialToken(tokens, Special.SUB)) {
            tokens.pop();
            negative = true;
        }
        literal = ((Literals.LiteralToken<?>) tokens.pop());

        if(negative) {
            if(literal.getType().equals(Integer.class)) {
                Literals.LiteralToken<Integer> intLit = (Literals.LiteralToken<Integer>) literal;
                intLit.updateValue(-intLit.getValue());
                literal = intLit;
            } else if(literal.getType().equals(Float.class)) {
                Literals.LiteralToken<Float> floatLit = (Literals.LiteralToken<Float>) literal;
                floatLit.updateValue(-floatLit.getValue());
                literal = floatLit;
            } else {
                throw new ParseError("Cannot negate a non-number", tokens);
            }
        }
    }

    public Class<?> getType() {
        return literal.getType();
    }

    public Object getValue() {
        return literal.getValue();
    }
}
