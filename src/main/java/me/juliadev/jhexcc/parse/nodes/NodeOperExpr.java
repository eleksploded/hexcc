package me.juliadev.jhexcc.parse.nodes;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class NodeOperExpr implements NodeExpr {
    NodeExpr left;
    NodeOper op;
    NodeExpr right;


}
