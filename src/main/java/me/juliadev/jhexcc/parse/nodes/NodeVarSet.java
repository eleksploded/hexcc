package me.juliadev.jhexcc.parse.nodes;

import lombok.Data;
import lombok.EqualsAndHashCode;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.Special;
import me.juliadev.jhexcc.parse.ExprParser;
import me.juliadev.jhexcc.parse.ParseUtil;

import java.util.LinkedList;

@Data
@EqualsAndHashCode(callSuper = false)
public class NodeVarSet extends NodeStmtList.NodeStmtParam {
    NodeIdent ident;
    NodeExpr value;

    NodeVarSet(LinkedList<Token> tokens) {
        ident = new NodeIdent(tokens);
        ParseUtil.pop(tokens, Special.ASSIGN);
        value = ExprParser.parse(tokens);
    }

    private NodeVarSet(NodeIdent id, NodeExpr value) {
        this.ident = id;
        this.value = value;
    }

    public static NodeVarSet makeDummy(NodeIdent id, NodeExpr value) {
        return new NodeVarSet(id, value);
    }
}
