package me.juliadev.jhexcc.parse.nodes;

import lombok.Data;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.parse.ParseError;
import me.juliadev.jhexcc.util.Pair;

import java.util.*;

@Data
public class NodePgrm {
    List<NodeDec> dec;
    public static final Map<Integer, String> filenameLookup = new HashMap<>();
    
    public NodePgrm(List<Pair<String, List<Token>>> tokens) {
        dec = new ArrayList<>();
        
        for (Pair<String, List<Token>> fileTokens : tokens) {
            LinkedList<Token> tokenQueue = new LinkedList<>(fileTokens.getRight());
            while(!tokenQueue.isEmpty()) {
                try {
                    filenameLookup.put(dec.size(), fileTokens.getLeft());
                    
                    ParseError.totalTokenLength = tokenQueue.size();
                    ParseError.currentFileId = dec.size();
                    
                    dec.add(new NodeDec(tokenQueue));
                } catch (IndexOutOfBoundsException e) {
                    throw new ParseError("unexpected EOF", tokenQueue);
                }
            }
        }
        
        
    }
}
