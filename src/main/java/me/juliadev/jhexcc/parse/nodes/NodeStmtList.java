package me.juliadev.jhexcc.parse.nodes;


import lombok.Data;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.IdentifierToken;
import me.juliadev.jhexcc.lex.tokens.Keywords;
import me.juliadev.jhexcc.lex.tokens.Special;
import me.juliadev.jhexcc.lex.tokens.TypeToken;
import me.juliadev.jhexcc.parse.ParseError;
import me.juliadev.jhexcc.parse.ParseUtil;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Data
public class NodeStmtList {
    List<NodeStmtParam> stmts;

    NodeStmtList(LinkedList<Token> tokens) {
        ParseUtil.pop(tokens, Special.OPEN_BRACE);

        stmts = new ArrayList<>();
        while(!ParseUtil.isSpecialToken(tokens, Special.CLOSE_BRACE)) {
            stmts.add(parseStmt(tokens));
        }
        tokens.pop();
    }

    private NodeStmtParam parseStmt(LinkedList<Token> tokens) {
        if(ParseUtil.isKeywordToken(tokens, Keywords.IF)) {
            return new NodeConditional(tokens);
        } else if(tokens.peek() instanceof TypeToken) {
            return new NodeVarDec(tokens);
        } else if(tokens.peek() instanceof IdentifierToken) {
            if(tokens.get(1) instanceof Special.SpecialToken s && s.getWord() == Special.OPEN_PAREN) {
                NodeFuncCall c = new NodeFuncCall(tokens);
                ParseUtil.pop(tokens, Special.SEMI);
                return c;
            } else if(tokens.get(1) instanceof Special.SpecialToken s && s.getWord() == Special.ASSIGN) {
                NodeVarSet nodeVarSet = new NodeVarSet(tokens);
                ParseUtil.pop(tokens, Special.SEMI);
                return nodeVarSet;
            }
        } else if(ParseUtil.isKeywordToken(tokens, Keywords.FOREACH)) {
            return new NodeIter(tokens);
        } else if(ParseUtil.isKeywordToken(tokens, Keywords.RETURN)) {
            return new NodeReturn(tokens);
        } else if(ParseUtil.isKeywordToken(tokens, Keywords.RAW)) {
            return new NodeRawStmt(tokens);
        }

        throw new ParseError("Unable to determine statement type", tokens);
    }

    public static abstract class NodeStmtParam {}
}
