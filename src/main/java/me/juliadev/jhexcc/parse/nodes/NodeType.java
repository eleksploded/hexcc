package me.juliadev.jhexcc.parse.nodes;

import lombok.Data;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.TypeToken;
import me.juliadev.jhexcc.parse.ParseError;

import java.util.LinkedList;

@Data
public class NodeType {
    TypeToken.Types type;

    NodeType(LinkedList<Token> tokens) {
        if(isTypeNext(tokens)) {
            type = ((TypeToken) tokens.pop()).type();
        } else {
            throw new ParseError("Invalid type", tokens);
        }
    }

    public static boolean isTypeNext(LinkedList<Token> tokens) {
        return tokens.peek() instanceof TypeToken;
    }
}
