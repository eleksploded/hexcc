package me.juliadev.jhexcc.parse;

import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.parse.nodes.NodePgrm;
import me.juliadev.jhexcc.util.Pair;

import java.util.List;

public class Parser {
    public static NodePgrm parse(List<Pair<String, List<Token>>> tokenStreams) {
        return new NodePgrm(tokenStreams);
    }
}
