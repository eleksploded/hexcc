package me.juliadev.jhexcc.parse;

import me.juliadev.jhexcc.HexCCError;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.parse.nodes.NodePgrm;

import java.util.Queue;

public class ParseError extends HexCCError {
    public static int totalTokenLength = 0;
    public static int currentFileId = 0;
    
    Queue<Token> tokens;
    String msg;
    
    public ParseError(String msg, Queue<Token> tokens) {
        this.msg = msg;
        this.tokens = tokens;
    }
    
    private static final String TEMPLATE = "A Parse error occurred.";
    
    @Override
    public String getMessage() {
        if(tokens.peek() == null) return (msg == null ? TEMPLATE : msg) + "\n    At EOF of " + NodePgrm.filenameLookup.get(currentFileId);
        return getMessage(NodePgrm.filenameLookup.get(0), (msg == null ? TEMPLATE : msg), tokens.peek().getLine(), tokens.peek().getColumn());
    }
}
