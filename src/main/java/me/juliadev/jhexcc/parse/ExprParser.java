package me.juliadev.jhexcc.parse;

import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.Literals;
import me.juliadev.jhexcc.lex.tokens.Special;
import me.juliadev.jhexcc.parse.nodes.*;
import me.juliadev.jhexcc.util.Pair;

import java.util.LinkedList;
import java.util.Map;

public class ExprParser {
    private static final Map<Special, Integer> operator_weight = Pair.makeMap(
            Pair.of(Special.PLUS, 3),
            Pair.of(Special.STAR, 4),
            Pair.of(Special.SUB, 3),
            Pair.of(Special.DIV, 4),
            Pair.of(Special.NOT, 2),
            Pair.of(Special.MOD, 4),
            Pair.of(Special.AND, 1),
            Pair.of(Special.OR, 1),
            Pair.of(Special.EQU, 0),
            Pair.of(Special.SHIFT_RIGHT, 5),
            Pair.of(Special.SHIFT_LEFT, 5),
            Pair.of(Special.GT, 0),
            Pair.of(Special.LT, 0),
            Pair.of(Special.GTE, 0),
            Pair.of(Special.LTE, 0),
            Pair.of(Special.NOT_EQU, 0)
    );

    public static NodeExpr parse(LinkedList<Token> tokens) {
        return parse(tokens, 0);
    }

    private static NodeExpr parse(LinkedList<Token> tokens, int minPrec) {
        NodeExpr lhs = parseTerm(tokens);

        while(true) {
            Token currentToken = tokens.peek();

            if(!isBiOp(currentToken) || getPrec(currentToken) < minPrec) break;
            int prec = getPrec(currentToken);

            int nextMin = prec + 1;
            NodeOper oper = new NodeOper(tokens);

            NodeExpr rhs = parse(tokens, nextMin);

            lhs = new NodeOperExpr(lhs, oper, rhs);
        }
        return lhs;
    }

    public static boolean isBiOp(Token currentToken) {
        if (currentToken instanceof Special.SpecialToken sp) {
            return operator_weight.containsKey(sp.getWord());
        }
        return false;
    }

    private static int getPrec(Token currentToken) {
        return operator_weight.get(((Special.SpecialToken) currentToken).getWord());
    }

    private static NodeExpr parseTerm(LinkedList<Token> tokens) {
        //literal, identifer, function call
        if(tokens.peek() instanceof Literals.LiteralToken<?>) {
            return new NodeLiteral(tokens);
        }
        if(tokens.peek() instanceof Special.SpecialToken s && s.getWord() == Special.SUB && tokens.get(1) instanceof Literals.LiteralToken<?>) {
            return new NodeLiteral(tokens);
        }

        if(tokens.peek() instanceof Special.SpecialToken s && s.getWord() == Special.OPEN_BRACE) {
            return new NodeVector(tokens);
        }

        if(tokens.peek() instanceof Special.SpecialToken s && s.getWord() == Special.OPEN_SQUARE_BRACE) {
            return new NodeList(tokens);
        }
        
        if(tokens.get(1) instanceof Special.SpecialToken s && s.getWord() == Special.OPEN_TRI_BRACE) {
            return new NodeIndex(tokens);
        }

        if(tokens.get(1) instanceof Special.SpecialToken s && s.getWord() == Special.OPEN_PAREN) {
            return new NodeFuncCall(tokens);
        }
        
        if(tokens.peek() instanceof Special.SpecialToken s && s.getWord() == Special.OPEN_PAREN) {
            ParseUtil.pop(tokens, Special.OPEN_PAREN);
            NodeExpr parse = parse(tokens);
            ParseUtil.pop(tokens, Special.CLOSE_PAREN);
            return parse;
        }
        return new NodeIdent(tokens);
    }

}
