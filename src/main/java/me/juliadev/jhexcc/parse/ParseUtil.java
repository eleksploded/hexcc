package me.juliadev.jhexcc.parse;

import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.Keywords;
import me.juliadev.jhexcc.lex.tokens.Special;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class ParseUtil {

    public static Token peekAheadUntil(LinkedList<Token> tokens, Token... options) {
        Set<Token> opt = new HashSet<>(Arrays.asList(options));
        
        for (Token token : tokens) {
            if (opt.contains(token)) {
                return token;
            }
        }
        return null;
    }

    public static boolean isSpecialToken(LinkedList<Token> token, Special special) {
        return token.peek() instanceof Special.SpecialToken s && s.getWord() == special;
    }

    public static boolean isKeywordToken(LinkedList<Token> tokens, Keywords keywords) {
        return tokens.peek() instanceof Keywords.KeywordToken s && s.getWord() == keywords;
    }

    public static boolean pop(LinkedList<Token> tokens, Special special) {
        if(isSpecialToken(tokens, special)) {
            tokens.pop();
            return true;
        } else {
            throw new ParseError("Expected '" + special.getSymbol() + "' symbol, but got " + tokens.peek().stringyVersion(), tokens);
        }
    }

    public static boolean pop(LinkedList<Token> tokens, Keywords special) {
        if(isKeywordToken(tokens, special)) {
            tokens.pop();
            return true;
        } else {
            throw new ParseError("Expected " + special.name() + ", but got " + tokens.peek().stringyVersion(), tokens);
        }
    }
}
