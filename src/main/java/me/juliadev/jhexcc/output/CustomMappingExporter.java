package me.juliadev.jhexcc.output;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomMappingExporter {
    public static void map(List<String> hex, File mappingFile) throws IOException {
        if(!mappingFile.exists()) {
            List<String> m = new ArrayList<>();
            hex.stream().map(s -> s + "=").forEach(s -> {
                if(!m.contains(s)) m.add(s);
            });
            Files.write(mappingFile.toPath(), m);
        } else {
            Map<String, String> mappings = new HashMap<>();
            for (String mapping : Files.readAllLines(mappingFile.toPath())) {
                String[] split = mapping.split("=");
                mappings.put(split[0], split[1]);
            }
            
            for (int i = 0; i < hex.size(); i++) {
                if(!mappings.containsKey(hex.get(i))) {
                    throw new FormatterError("No mappings for hex: " + hex.get(i), CustomMappingExporter.class);
                }
                hex.set(i, mappings.get(hex.get(i)));
            }
        }
    }
}
