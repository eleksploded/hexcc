package me.juliadev.jhexcc.output;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PatternExporter {
    private static final Map<String, String> patternMap = new HashMap<>();
    private static final String PATTERN_CSV = "https://hexxy.media/patterns.csv";
    private static final String NUMBERS_JSON = "https://raw.githubusercontent.com/object-Object/HexBug/main/src/HexBug/hexdecode/numbers_2000.json";
    
    public static void map(List<String> hex) throws IOException {
        InputStream in = new URL(PATTERN_CSV).openStream();
        String patterns = StandardCharsets.UTF_8.decode(ByteBuffer.wrap(in.readAllBytes())).toString();
        in.close();
        
        for (String patternLine : patterns.split("\n")) {
            String[] data = patternLine.split(",");
            patternMap.put(data[1], data[2] + " " + data[3]);
        }
        patternMap.put("Iris' Gambit", "NORTH_WEST qwaqde");
        
        for(int i = 0; i < hex.size(); i++) {
            if(hex.get(i).startsWith("Numerical Reflection: ")) {
                String num = hex.get(i).substring("Numerical Reflection: ".length());
                hex.set(i, getNumber(Integer.parseInt(num)));
                continue;
            }

            if(hex.get(i).startsWith("Bookkeepers Gambit: ")) {
                hex.set(i, hex.get(i).replace("Bookkeepers", "Bookkeeper's"));
            }

            if(hex.get(i).startsWith("Bookkeeper's Gambit: ")) {
                String bk = hex.get(i).substring("Bookkeeper's Gambit: ".length());
                hex.set(i, getBookkeepers(bk));
                continue;
            }
            
            if(!patternMap.containsKey(hex.get(i))) throw new FormatterError("Unknown Hex: " + hex.get(i), PatternExporter.class);
            hex.set(i, patternMap.get(hex.get(i)));
        }
    }
    
    private static String numbersData;
    private static String getNumber(int num) throws IOException {
        if(numbersData == null) {
            InputStream in = new URL(NUMBERS_JSON).openStream();
            numbersData = StandardCharsets.UTF_8.decode(ByteBuffer.wrap(in.readAllBytes())).toString();
            in.close();
        }
        
        if(num < -2000 || num > 2000) throw new FormatterError("Cannot convert number: " + num, PatternExporter.class);
        
        //this is horrible but i didnt want to depend on a json lib just for this
        String numStr = "\"" + num + "\"";
        int numIndex = numbersData.indexOf(numStr);
        numIndex += numStr.length() + (":[\"".length());
        int end = numbersData.indexOf("]", numIndex);
        String patternData = numbersData.substring(numIndex, end - 1);
        return patternData.replaceAll("\",\"", " ");
    }
    
    //https://github.com/object-Object/HexBug/blob/22b83ea855e4ece16ad132f3ad366d59007eafb6/src/HexBug/hexdecode/hexast.py#L216
    private static String getBookkeepers(String mask) {
        if(!mask.chars().allMatch(c -> c == 'v' || c == '-')) {
            throw new FormatterError("Invalid bookkeepers mask: " + mask, FormatterError.class);
        }
        
        StringBuilder pattern = new StringBuilder();
        
        if(mask.charAt(0) == 'v') pattern.append("SOUTH_EAST a");
        else if(mask.charAt(0) == '-') pattern.append("EAST ");
        
        for (int i = 1; i < mask.toCharArray().length; i++) {
            char prev = mask.charAt(i - 1);
            char curr = mask.charAt(i);
            
            if(prev == '-' && curr == '-') {
                pattern.append('w');
            } else if(prev == '-' && curr == 'v') {
                pattern.append("ea");
            } else if(prev == 'v' && curr == '-') {
                pattern.append('e');
            } else if(prev == 'v' && curr == 'v') {
                pattern.append("da");
            }
        }
        
        return pattern.toString();
    }
}
