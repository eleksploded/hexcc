package me.juliadev.jhexcc.output;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class FormatterError extends RuntimeException {
    String msg;
    Class<?> formatter;

    @Override
    public String getMessage() {
        return "Error in " + formatter.getName() + ": " + msg;
    }
}
