package me.juliadev.jhexcc;

public class HexCCError extends RuntimeException {
    public static final int CONTEXT_WIDTH = 16;

    public String getMessage(String file, String error, int line, int column) {
        String fileData = InputFiles.readAllLines(file).get(line);
        int beginningContext = Math.max(0, column - CONTEXT_WIDTH);
        int endingContext = Math.min(fileData.length(), column + CONTEXT_WIDTH);

        String lineContext = fileData.substring(beginningContext, endingContext);
        String arrow = "-".repeat(column - beginningContext) + "^" + "-".repeat(endingContext - column - 1);
        String lineHeader = " at " + file + ":" + (line+1) + " -> ";

        return error + "\n" + lineHeader + lineContext + "\n" + " ".repeat(lineHeader.length()) + arrow;
    }

    @Override
    public String toString() {
        String s = getClass().getName();
        String message = getLocalizedMessage().split("\n")[0];
        return (message != null) ? (s + ": " + message) : s;
    }
}
