import me.juliadev.jhexcc.lex.Lexical;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.lex.tokens.IdentifierToken;
import me.juliadev.jhexcc.lex.tokens.Keywords;
import me.juliadev.jhexcc.lex.tokens.Literals;
import me.juliadev.jhexcc.lex.tokens.Special;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class LexicalTests {
    @Test
    public void test_lexical_empty() {
        assertTrue(Lexical.tokenize("",new char[]{}).getRight().isEmpty());
    }
    
    @Test
    public void test_lexical_normal() {
        String src = """
                void main() {
                    entity caster = getCaster();
                    vec look = getLook(caster);
                    impulse(caster, look * 2);
                }
                """;
        List<Token> expectedTokens = List.of(
                Keywords.VOID.makeToken(0,0), new IdentifierToken("main", 0, 5), Special.OPEN_PAREN.makeToken(0,9), Special.CLOSE_PAREN.makeToken(0,10), Special.OPEN_BRACE.makeToken(0,12),
                Keywords.ENT.makeToken(1,4), new IdentifierToken("caster",1,11), Special.ASSIGN.makeToken(1,18), new IdentifierToken("getCaster",1,20), Special.OPEN_PAREN.makeToken(1,29), Special.CLOSE_PAREN.makeToken(1,30), Special.SEMI.makeToken(1,31),
                Keywords.VEC.makeToken(2,4), new IdentifierToken("look",2,8), Special.ASSIGN.makeToken(2,13), new IdentifierToken("getLook",2,15), Special.OPEN_PAREN.makeToken(2,22), new IdentifierToken("caster",2,23), Special.CLOSE_PAREN.makeToken(2,29), Special.SEMI.makeToken(2,30),
                new IdentifierToken("impulse",3,4), Special.OPEN_PAREN.makeToken(3,11), new IdentifierToken("caster",3,12), Special.COMMA.makeToken(3,18), new IdentifierToken("look",3,20), Special.STAR.makeToken(3,25), Literals.INT.makeToken(2,3,27), Special.CLOSE_PAREN.makeToken(3,28), Special.SEMI.makeToken(3,29),
                Special.CLOSE_BRACE.makeToken(4,0)
        );
        List<Token> tokens = Lexical.tokenize("", src.toCharArray()).getRight();
        assertFalse(tokens.isEmpty());

        assertEquals(expectedTokens.size(), tokens.size());
        for(int i = 0; i < expectedTokens.size(); ++i) {
            assertTrue("Differ at token %d. Expected: %s, got: %s".formatted(i, expectedTokens.get(i), tokens.get(i)), areTokensEqualWithIndices(expectedTokens.get(i), tokens.get(i)));
        }
    }
    
    private static boolean areTokensEqualWithIndices(Token a, Token b) {
        return a.equals(b) && a.getColumn() == b.getColumn() && a.getLine() == b.getLine();
    }
}
