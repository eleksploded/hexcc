import me.juliadev.jhexcc.InputFiles;
import me.juliadev.jhexcc.codegen.hex.HexPattern;
import me.juliadev.jhexcc.codegen.hex.generators.HexGenerator119;
import me.juliadev.jhexcc.codegen.hex.generators.HexGenerator1201;
import me.juliadev.jhexcc.codegen.ir_hexc.IrHexCProgram;
import me.juliadev.jhexcc.lex.Lexical;
import me.juliadev.jhexcc.lex.Token;
import me.juliadev.jhexcc.optimization.Optimization;
import me.juliadev.jhexcc.optimization.Optimizer;
import me.juliadev.jhexcc.parse.Parser;
import me.juliadev.jhexcc.parse.nodes.NodePgrm;
import me.juliadev.jhexcc.util.Pair;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TestStuff {

    public static void main(String[] argsArray) {
        List<String> args = List.of(argsArray);
        System.out.print("\n---------- Source ----------\n\n");

        String src = """
                entity getCaster() {
                    entity caster;
                    raw {
                        Mind's Reflection
                        #(caster)
                    }
                    return caster;
                }
                
                void impulse(entity target, vec direction) {
                    raw {
                        $(target)
                        $(direction)
                        Impulse
                    }
                }
                
                vec getLocation(entity target) {
                    vec loc;
                    raw {
                        $(target)
                        Compass Purification II
                        #(loc)
                    }
                    return loc;
                }
                
                list nearbyMobs() {
                    list mobs;
                    vec center = getLocation(getCaster());
                    raw {
                        $(center)
                        Numerical Reflection: 16
                        Zone Distillation: Monster
                        #(mobs)
                    }
                    return mobs;
                }
                
                vec normalize(vec v) {
                    int length;
                    raw {
                        $(v)
                        Length Purification
                        #(length)
                    }
                    return v / length;
                }
                
                void main() {
                    list vectors = [{1,0,0}, {-1,0,0}, {0,1,0}];
                    entity caster = getCaster();
                    vec casterLocation = getLocation(caster);
                    for(entity mob in nearbyMobs()) {
                        vec loc = getLocation(mob);
                        vec dir = normalize(loc - casterLocation) + vectors<vec>[2];
                        impulse(mob, 2 * dir);
                    }
                }
                """;
        System.out.println(src);
        try {
            Field fileData = InputFiles.class.getDeclaredField("fileData");
            fileData.setAccessible(true);
            ((Map<String, List<String>>) fileData.get(null)).put("test", Arrays.stream(src.split("\n")).toList());
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }

        System.out.print("\n---------- Tokens ----------\n\n");

        Pair<String, List<Token>> tokens = Lexical.tokenize("test", src.toCharArray());
        System.out.println(tokens.getRight());//tokens.stream().map(Object::getClass).map(Class::getSimpleName).toList());

        System.out.print("\n---------- Parse Tree ----------\n\n");

        NodePgrm pgrm = Parser.parse(List.of(tokens));
        printTree(pgrm);

        System.out.print("\n---------- IR-HexC ----------\n\n");

        IrHexCProgram irhexc = IrHexCProgram.generate(pgrm);
        System.out.println(String.join("\n", irhexc.getActions().stream().map(Object::toString).toList()));

        // ---------------- Optimize ----------------------

        if(!args.contains("--skipOptimizations")) {
            System.out.print("\n---------- Optimize ----------\n\n");
            List<String> list = Optimizer.getAllOptimizations(null).stream().map(Optimization::getName).toList();
            Optimizer.runOptimizations(list, null, irhexc, -1);
            System.out.println(String.join("\n", irhexc.getActions().stream().map(Object::toString).toList()));
        }
        
        System.out.print("\n---------- Trace ----------\n\n");
        
        //TraceIR.trace(irhexc);
        System.out.println("not tracing yet");

        if(!args.contains("--skipHexGeneration")) {
            System.out.print("\n---------- Hex ----------\n\n");
            List<String> gen = HexGenerator1201.generate(irhexc).stream().map(HexPattern::getHexString).collect(Collectors.toCollection(ArrayList::new));
            System.out.println(String.join("\n", gen));
        }
    }

    private static void printTree(NodePgrm pgrm) {
        try {
            System.out.println(pgrm.getClass().getSimpleName());
            printTree(pgrm, "  ");
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    private static void printTree(Object obj, String ident) throws ReflectiveOperationException {
        final String pkg = NodePgrm.class.getName().substring(0, NodePgrm.class.getName().lastIndexOf("."));
        final String indentTkn = "  ";

        if(obj == null) {
            System.out.println(ident + "NULL");
        } else if(obj.getClass().getName().startsWith(pkg)) {
            for (Field field : obj.getClass().getDeclaredFields()) {
                if(Modifier.isStatic(field.getModifiers())) continue;

                field.setAccessible(true);
                Object value = field.get(obj);
                String type = value != null ? value.getClass().getSimpleName() : field.getType().getSimpleName();

                if(field.getType().getGenericSuperclass() instanceof ParameterizedType t) {
                    Class<?> clazz = (Class<?>) t.getActualTypeArguments()[0];
                    type += ("<" + clazz.getSimpleName() + ">");
                }

                System.out.println(ident + field.getName() + " (" + type + ")");
                printTree(value, ident + indentTkn);
            }
        } else if(obj instanceof List<?> list) {
            if(list.isEmpty()) System.out.println(ident + "*empty*");
            for (int i = 0; i < list.size(); i++) {
                Object o = list.get(i);
                System.out.println(ident + i + " - " + o.getClass().getSimpleName());
                printTree(o, ident + indentTkn);
            }
        } else {
            System.out.println(ident + obj);
        }
    }
}
